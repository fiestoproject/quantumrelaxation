<?php

if($_GET['testing']!="") {
	$_SESSION['testing']=$_GET['testing'];
}
/*
$action = fiestolaundry($_GET['action'],20);
$pid = fiestolaundry($_GET['pid'],11);

if ($action=='') {
	header("Location:$cfg_app_url");
}

if ($action=='view') {
	if ($pid!='') {
		$sql = "SELECT judul, isi FROM page WHERE id='$pid'";
		$result = $mysql->query($sql);
		list($title,$content)=$mysql->fetch_row($result);
	}
}
*/

if($_POST['submit'] and ($action=='' or $action=='order')) {
	
	$valid=true;
	$nama_pemesan=cleanInput($_POST['nama_pemesan']);
	$nama_panggilan=cleanInput($_POST['nama_panggilan']);
	$jenis_kelamin=cleanInput($_POST['jenis_kelamin']);
	$wa_pemesan=cleanInput($_POST['wa_pemesan'],'numeric');
	$email_pemesan=cleanInput($_POST['email_pemesan'],'email');
	if($nama_pemesan=="") {
		$msg_error[]='Nama pemesan harus diisi';
		$valid=false;
	}
	if($nama_pemesan!="" and cleanInput($_POST['nama_pemesan'],'alpha')=="")
	{
		$msg_error[]='Nama pemesan tidak valid';
		$valid=false;
	}
	if($nama_panggilan=="") {
		$msg_error[]='Nama panggilan harus diisi';
		$valid=false;
	}
	if($nama_panggilan!="" and cleanInput($_POST['nama_panggilan'],'alpha')=="")
	{
		$msg_error[]='Nama panggilan tidak valid';
		$valid=false;
	}
	if($jenis_kelamin=="") {
		$msg_error[]='Jenis kelamin harus diisi';
		$valid=false;
	}
	if($wa_pemesan!="" AND strlen($wa_pemesan)<9) {
		$msg_error[]='Nomor WA tidak valid';	
		$valid=false;
	}
	
	if($email_pemesan=="") {
		$msg_error[]='Email harus diisi';
		$valid=false;
	}
	if($email_pemesan!="" AND !isValidEmail($email_pemesan)) {
		$msg_error[]='Email tidak valid';
		$valid=false;
	}
	
	$_SESSION['nama_pemesan']=$nama_pemesan;
	$_SESSION['nama_panggilan']=$nama_panggilan;
	$_SESSION['jenis_kelamin']=$jenis_kelamin;
	$_SESSION['wa_pemesan']=$wa_pemesan;
	$_SESSION['email_pemesan']=$email_pemesan;
	$nominal = $qmf_relaxation_harga;

	if(count($msg_error)>0) {
			$_SESSION['msg']='
			<div class="alert alert-danger" role="alert">
				<ul><li>'.join('</li><li>',$msg_error).'</li></ul>
			</div>
			';
	} else {
		unset($_SESSION['msg']);
	}
	
	if(!$valid) {
		header("location://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		exit();
	}
	if($valid) {
		
		
		$waktu_sekarang=date("Y-m-d H:i:s");
		$uniqid=uniqid();
		$auto=md5($uniqid.$waktu_sekarang.$nama_pemesan.$panggilan.$wa_pemesan.$email_pemesan.$jenis_kelamin);
		$q = $mysql->query("
		INSERT INTO 
			qmf_relaxation_order
				SET 
					auto='$auto',
					tanggal_order='$waktu_sekarang',
					nama_pemesan='$nama_pemesan',
					nama_panggilan='$nama_panggilan',
					jenis_kelamin='$jenis_kelamin',
					wa_pemesan='$wa_pemesan',
					email_pemesan='$email_pemesan',
					nominal='$nominal'
		");
		
		if(!$q) {
			$valid=false;
			$msg_error[] = "Gagal input kedalam sistem silahkan ulangi kembali";
		}
		//ambil id terakhir
		
		$q_id =  $mysql->query(" SELECT id FROM qmf_relaxation_order WHERE auto='$auto' ");
		list($id_order) =  $mysql->fetch_row($q_id);
		
		$nominal_unik=($id_order%900)+100;
		$total=$nominal+$nominal_unik;
		$_SESSION['nominal_unik']=$nominal_unik;
		$_SESSION['nominal']=$nominal;
		$_SESSION['total']=$total;
		//generate kode
		$kode=generate_booking_code($id_order);
		$update = $mysql->query("UPDATE  qmf_relaxation_order SET kode='$kode',nominal_unik=$nominal_unik,total=$total WHERE id=$id_order ");
		if(!$update) {
			$valid=false;
			$msg_error[] = "Gagal generate kode order";
		}
		//if($_SESSION['testing']==1) 
		{
		//BUAT INFO NOTA/
		
			$_SESSION['order_id']=$kode;	
			$replace=array(
			'#id_order'=>$kode,
			'#nominal'=>number_format($nominal,0,',','.'),
			'#nominal_unik'=>$nominal_unik,
			'#total'=>number_format($total,0,',','.'),
			'#nama_pemesan'=>$nama_pemesan,
			'#nama_panggilan'=>$nama_panggilan,
			'#jenis_kelamin'=>$jenis_kelamin,
			'#bapak_ibu'=>($jenis_kelamin=='Laki-laki'?'Bapak':'Ibu'),
			'#waktu_sekarang'=>$waktu_sekarang,
			'#wa_pemesan'=>$wa_pemesan,
			'#email_pemesan'=>$email_pemesan
			);
			
	   /*KIRIM WA*/		
			if(count($replace)>0){
				foreach($replace as $i =>$v){
					$qmf_relaxation_wa_baru=str_replace($i,$v,$qmf_relaxation_wa_baru);
				}
			}
			
		
		//END BUAT INFO NOTA//
		
		//send wa to buyer
		
		//$log=send_wa($wa_pemesan,$qmf_relaxation_wa_baru);
		if($_SESSION['testing']==1) {
			//$log=send_wa($wa_pemesan,$qmf_relaxation_wa_baru);
			//$log=send_wa($wa_pemesan,$qmf_relaxation_wa_baru);
			//var_dump($log);
		//	die();
		}
		
		$update_status=$mysql->query("UPDATE qmf_relaxation_order SET wa_send_time='".date("Y-m-d H:i:s")."',  wa_log_message='".$log."' WHERE  id=$id_order ");
		
		/*END KIRIM WA*/
		
		/*KIRIM EMAIL*/
		
		$sql = "SELECT value FROM contact WHERE name='umbalemail'";
		$result = $mysql->query($sql);
		list($umbalemail) = $mysql->fetch_row($result);
		$r_email = explode(",",$umbalemail);
		
	
		if(count($replace)>0){
			foreach($replace as $i =>$v){
				$qmf_relaxation_email_baru=str_replace($i,$v,$qmf_relaxation_email_baru);
			}
		}
		
		$pesan_email=$qmf_relaxation_email_baru;
		//email to admin
		//$to, $subject, $txtmsg, $from, $namafrom='', $replyto='', $htmlmsg='', $attachments=''
		foreach($r_email as $email_admin) {
			$sukses=fiestophpmailer($email_admin,"$nama_pemesan ".number_format($total,0,',','.'),'',$smtpuser,$nama_pemesan,$email_pemesan,$pesan_email);
		}
		//email to buyer
		$sukses=fiestophpmailer($email_pemesan,"$nama_pemesan ".number_format($total,0,',','.'),'',$smtpuser,$nama_pemesan,$email_admin,$pesan_email);
		$update_status=$mysql->query("UPDATE qmf_relaxation_order SET email_send_time='".date("Y-m-d H:i:s")."', email_send_status='".$sukses."' WHERE  id=$id_order ");
		
		/*KIRIM EMAIL*/	
		
		if(count($msg_error)>0) {
			$_SESSION['msg']='
			<div class="alert alert-danger" role="alert">
				<ul><li>'.join('</li><li>',$msg_error).'</li></ul>
			</div>
			';
		} else {
			unset($_SESSION['msg']);
		}
		
		if(!$valid) {
			header("location://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			exit();
		} else {
			$_SESSION['sukses_order_id']=$_SESSION['order_id'];
			$_SESSION['sukses_nominal']=$_SESSION['nominal'];
			$_SESSION['sukses_nominal_unik']=$_SESSION['nominal_unik'];
			$_SESSION['sukses_total']=$_SESSION['total'];
			$_SESSION['sukses_jenis_kelamin']=$_SESSION['jenis_kelamin'];
			$_SESSION['sukses_nama_pemesan']=$_SESSION['nama_pemesan'];
			$_SESSION['sukses_nama_panggilan']=$_SESSION['nama_panggilan'];
			$_SESSION['sukses_wa_pemesan']=$_SESSION['wa_pemesan'];
			$_SESSION['sukses_email_pemesan']=$_SESSION['email_pemesan'];
			
			unset($_SESSION['nama_pemesan']);
			unset($_SESSION['nama_panggilan']);
			unset($_SESSION['jenis_kelamin']);
			unset($_SESSION['wa_pemesan']);
			unset($_SESSION['email_pemesan']);
			unset($_SESSION['msg']);
			unset($_SESSION['order_id']);

			header("location://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."?order=sukses");
			exit();
		}
		
		}
		
		

	}
}




if( ($action=='' or $action=='order') and $_GET['order']!="sukses") {
$data=array();
if($_SESSION['member_uname']!='') {
	
	
	$q=$mysql->query("SELECT 	user_email,fullname,cellphone1,initial,gender FROM webmember WHERE username='".$_SESSION['member_uname']."'	");
	$data=$mysql->fetch_assoc($q);
}	
$title="Form Pemesanan";
$nominal=$_SESSION['nominal'];
$nominal_unik=$_SESSION['nominal_unik'];
$total=$_SESSION['total'];
$nama_pemesan=$_SESSION['nama_pemesan']==''?$data['fullname']:$_SESSION['nama_pemesan'];
$nama_panggilan=$_SESSION['nama_panggilan']==''?$data['initial']:$_SESSION['nama_panggilan'];
$jenis_kelamin=$_SESSION['jenis_kelamin']==''?$data['gender']:$_SESSION['jenis_kelamin'];
$wa_pemesan=$_SESSION['wa_pemesan']==''?$data['cellphone1']:$_SESSION['wa_pemesan'];;
$email_pemesan=$_SESSION['email_pemesan']==''?$data['user_email']:$_SESSION['email_pemesan'];
$order_id=$_SESSION['order_id'];

$form.=$_SESSION['msg'];
$form.='
<form method="post" role="form" style="max-width:400px;margin:0 auto">
	<div class="form-group">
	<label for="nama_pemesan">Nama Lengkap</label>
	<input class="form-control" style="padding:0px 10px 0px 10px;" id="nama_pemesan" name="nama_pemesan" type="text" value="'.$nama_pemesan.'" required="required"/>
	</div>
	<div>
	<label for="nama_panggilan">Nama Panggilan</label>
	<input class="form-control" style="padding:0px 10px 0px 10px;" id="nama_panggilan" name="nama_panggilan" type="text" value="'.$nama_panggilan.'" required="required"/>
	</div>
	<div class="form-group">
	<br/>
	<label for="laki-laki"><input id="laki_laki" name="jenis_kelamin" type="radio" value="Laki-laki" '.($jenis_kelamin==1?'checked="checked"':'').' required="required"/> Laki Laki</label>
	<label for="perempuan"><input id="perempuan" name="jenis_kelamin" type="radio" value="Perempuan" '.($jenis_kelamin==2?'checked="checked"':'').' required="required"/> Perempuan</label>
	
	</div>
	<div class="form-group">
	<label for="wa_pemesan">Nomor Whatsapp</label>
	<input class="form-control" style="padding:0px 10px 0px 10px;" id="wa_pemesan" name="wa_pemesan" type="number" value="'.$wa_pemesan.'" required="required"/>
	</div>
	<div class="form-group">
	<label for="email_pemesan">Email</label>
	<input class="form-control" style="padding:0px 10px 0px 10px;" id="email_pemesan" name="email_pemesan" type="email" value="'.$email_pemesan.'" required="required"/>
	</div>
	<div>
	<input type="submit" class="btn btn-default more" name="submit" value="Beli">
	</div>
	<div>
	<br/>
	'.$qmf_relaxation_submit.'
	</div>
</form>
';
$content .="<div class='qmf_page'>".$qmf_relaxation_page."</div>";
$content .="$form";

}
if( ($action=='' or $action=='order') and $_GET['order']=="sukses"){
$title="Pemesanan Berhasil";	
$nominal=$_SESSION['sukses_nominal'];
$nominal_unik=$_SESSION['sukses_nominal_unik'];
$total=$_SESSION['sukses_total'];
$nama_pemesan=$_SESSION['sukses_nama_pemesan'];
$nama_panggilan=$_SESSION['sukses_nama_panggilan'];
$jenis_kelamin=$_SESSION['sukses_jenis_kelamin'];
$wa_pemesan=$_SESSION['sukses_wa_pemesan'];
$email_pemesan=$_SESSION['sukses_email_pemesan'];
$order_id=$_SESSION['sukses_order_id'];	
$replace=array(
'#id_order'=>$order_id,
'#nominal'=>number_format($nominal,0,',','.'),
'#nominal_unik'=>$nominal_unik,
'#total'=>number_format($total,0,',','.'),
'#nama_pemesan'=>$nama_pemesan,
'#nama_panggilan'=>$nama_panggilan,
'#jenis_kelamin'=>$jenis_kelamin,
'#bapak_ibu'=>($jenis_kelamin=='Laki-laki'?'Bapak':'Ibu'),
'#wa_pemesan'=>$wa_pemesan,
'#email_pemesan'=>$email_pemesan
);	

if(count($replace)>0){
	foreach($replace as $i =>$v){
		$qmf_relaxation_thank_you=str_replace($i,$v,$qmf_relaxation_thank_you);
	}
}	
	$content .="<div class='qmf_page' style='text-align:center'>".$qmf_relaxation_thank_you."</div>";
}
if ($action=='unduh') {
	$kode=cleanInput($_GET['kode']);

	$q = $mysql->query(" SELECT first_login FROM qmf_relaxation_order WHERE status_order=1 AND kode='$kode'  ");
	if($q and $mysql->num_rows($q)>0) {
		list($first_login) = $mysql->fetch_row($q);
		/*
		$date_a = new DateTime(date("Y-m-d H:i:s"));
		$date_b = strtotime('2020-08-15 11:58:58');
		*/
		
		$date_a = strtotime($first_login);
		$date_b = strtotime(date("Y-m-d H:i:s"));
		$interval = ($date_b-$date_a)/60/60;//jam
		
		if($interval>$qmf_relaxation_download_limit) {
			header("location:$cfg_app_url/".$_GET['p']);
			exit();
		} else {
			$sql = "SELECT filename FROM qmf_filedata WHERE md5(id)='$pid'";
			$result = $mysql->query($sql);
			list($filename) = $mysql->fetch_row($result);
			$mysql->query("UPDATE qmf_filedata SET clickctr=clickctr+1 WHERE id='$pid'");
			downloadFile("$cfg_file_path/$filename");
			//header("Location:$cfg_file_path/$filename");
		}
	}
	
}
if($action=="download") {
	
	$pid=cleanInput($pid);
	$kode=$_GET['kode'];
	$sekarang=date("Y-m-d H:i:s");
	$first_login = $mysql->query("UPDATE  qmf_relaxation_order SET first_login='$sekarang' WHERE status_order=1 AND kode='$kode' AND first_login='0000-00-00 00:00:00'");
	$q = $mysql->query(" SELECT first_login FROM qmf_relaxation_order WHERE status_order=1 AND kode='$kode'  ");
	if($q and $mysql->num_rows($q)>0) {
		list($first_login) = $mysql->fetch_row($q);
		$date_a = strtotime($first_login);
		$date_b = strtotime(date("Y-m-d H:i:s"));
	
		$interval = ($date_b-$date_a)/60/60;//jam
		
		if($interval<=$qmf_relaxation_download_limit) {
	
		$sql1 = "SELECT id, filename, filesize, title FROM qmf_filedata order by title";
		$result1 = $mysql->query($sql1);
		$content .= "<ul>\r\n";
		while (list($pid, $filename, $filesize, $filetitle) = $mysql->fetch_row($result1)) {
			if ($filetitle=='') $filetitle=$filename;
			
			$titleurl = array();
			$titleurl["pid"] = $filetitle;
				
			$content .= "<li class=\"xdata\"><a href=\"".$urlfunc->makePretty("?p=".$_GET['p']."&action=unduh&pid=".md5($pid)."&kode=$kode")."\">$filetitle</a> (".convertbyte($filesize).")</li>\r\n";
		}
		$content .= "</ul>\r\n";
		$content .="<div class=\'qmf_page\'>".$qmf_relaxation_download_text."</div>";
		}
		
		$content .="<div class=\'qmf_page\'>".$qmf_relaxation_download."</div>";
	} else {
		header("location:$cfg_app_url");
		exit();
	}
	
	
}

if($action=="login") {
	$check_login=$_POST['check_login'];
	$kode_unik=fiestolaundry($_POST['kode_unik']);
	$md5_kode_unik=md5($_POST['kode_unik']);
	$msg="";
	if($check_login) {
		$q=$mysql->query(" SELECT status_time FROM qmf_relaxation_order WHERE status_order=1 AND md5(kode)='$md5_kode_unik' ");
		if($q and $mysql->num_rows($q)>0) {
			header("location:$cfg_app_url/product/download/$kode_unik");
			exit();
		} else {
			$msg="<div class=\"alert alert-danger\">Password salah atau tidak ditemukan!</div>";
		}
	}
	
	$content .=<<<END
<div class="col-lg-12">
<div class="row"><div id="" class="col-xs-12 col-sm-6 col-lg-6 col-sm-push-3">
						$msg
						<form class="box" action="$cfg_app_url/product/login" id="loginform" name="loginform" method="POST">
							<h3 class="page-subheading">Silakan Masukkan Password:</h3>
							<div class="form_content clearfix">
								<div class="form-group">
									<label for="username">Password</label>
									<input type="text" value="" name="kode_unik" class="is_required validate account_input form-control">
								</div>
								
								
								<table border="0">
								<tbody><tr>
								<td>
								<p class="submit">		
									<input type="hidden" name="check_login" value="1">
									<button class="more button btn btn-default button-medium" name="submit_login" type="submit">
										<span>
											<i class="fa fa-lock left"></i>
											Login
										</span>
									</button>
								</p></td><td>&nbsp;&nbsp;</td>
								<td valign="top"><font color="red"></font></td></tr>
								</tbody></table>
								<p></p>
							</div>
						</form>
					</div>
			
		</div>	
	</div>
END;

}

/*
unset($_SESSION['nama_pemesan']);
unset($_SESSION['wa_pemesan']);
unset($_SESSION['email_pemesan']);
unset($_SESSION['msg']);
unset($_SESSION['order_id']);
*/
?>
