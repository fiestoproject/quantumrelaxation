<?php
if($_GET['testing']!="") {
	$_SESSION['testing']=$_GET['testing'];
}
$waktu_sekarang=date("Y-m-d H:i:s");
if (!$isloadfromindex) {
	include ("../../kelola/urasi.php");
	include ("../../kelola/fungsi.php");
	include ("../../kelola/lang/$lang/definisi.php");
	pesan(_ERROR,_NORIGHT);
}
$max_page_list=100;
$keyword=fiestolaundry($_GET['keyword'],100);
$screen=fiestolaundry($_GET['screen'],11);
$pid=fiestolaundry($_REQUEST['pid'],11);

$isi = fiestolaundry($_POST['isi'],0,TRUE);
$action = fiestolaundry($_REQUEST['action'],10);
$url = fiestolaundry($_POST['url'],255);


if ($action == '') {
	$admincontent .= "
		<ul>
			<li><a href=\"?p=product&action=step1\">"._LISTORDERPROCESSPAY."</a></li>
			<li><a href=\"?p=product&action=step2\">"._LISTORDERPROCESSSENT."</a></li>
		</ul>
	";
}
if ($action=='step1' || $action=='step2') {
	if($action=='step1') {
		$kondisi='status_order=0';
		$prmtr="action=step1";
		$admintitle=_LISTORDERPROCESSPAY;
	}
	if($action=='step2') {
		$kondisi='status_order=1';
		$prmtr="action=step1";
		$order_by='ORDER BY tanggal_order DESC';
		$admintitle=_LISTORDERPROCESSSENT;
	}
	$sql = "SELECT id FROM qmf_relaxation_order WHERE $kondisi ";

	$result = $mysql->query($sql);
	$total_records = $mysql->num_rows($result);
	$pages = ceil($total_records/$max_page_list);
	
	if ($mysql->num_rows($result) > 0) {
		$start = $screen * $max_page_list;
		$sql = "SELECT id,kode,tanggal_order,nama_pemesan,nama_panggilan,jenis_kelamin,wa_pemesan,email_pemesan,total,status_order FROM qmf_relaxation_order WHERE $kondisi $order_by LIMIT $start, $max_page_list";
		$result = $mysql->query($sql);

		if ($pages>1) $adminpagination = pagination($namamodul,$screen,$prmtr);
		$admincontent .= "<table class=\"list\" border=\"1\" cellspacing=\"1\" cellpadding=\"6\">\n";
		$admincontent .= "<tr><th>Tanggal</th>";
		$admincontent .= "<th>Kode</th>";
		$admincontent .= "<th>Nama</th>";
		$admincontent .= "<th>Panggilan</th>";
		$admincontent .= "<th>Jenis Kelamin</th>";
		$admincontent .= "<th>WA</th>";
		$admincontent .= "<th>Email</th>";
		$admincontent .= "<th>Nominal</th>";
		$admincontent .= "<th>Status</th>";
		$admincontent .= "<th>"._EDIT."</th><th>"._DEL."</th></tr>\n";
		while (list($id, $kode,$tanggal_order,$nama_pemesan,$nama_panggilan,$jenis_kelamin,$wa_pemesan,$email_pemesan,$total, $status_order) = $mysql->fetch_row($result)) {
			$admincontent .= "<tr>\n";
			$admincontent .= "<td>$tanggal_order</td>";
			$admincontent .= "<td>$kode</td>";
			$admincontent .= "<td>$nama_pemesan</td>";
			$admincontent .= "<td>$nama_panggilan</td>";
			$admincontent .= "<td>$jenis_kelamin</td>";
			$admincontent .= "<td>$wa_pemesan</td>";
			$admincontent .= "<td>$email_pemesan</td>";
			$admincontent .= "<td>$total</td>";
			$admincontent .= "<td>".($status_order==1?"Sudah Bayar":"Belum Bayar")."</td>";
			$admincontent .= "<td align=\"center\"><a href=\"?p=product&action=modify&pid=$id\">";
			$admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
			$admincontent .= "<td align=\"center\"><a href=\"?p=product&action=remove&pid=$id\">";
			$admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
			$admincontent .= "</tr>\n";
		}
		$admincontent .= "</table>";
		
	} else {
		$admincontent = "<p>Tidak ada order</p>";
		
	}

}

if ($action=='search') {
    $admintitle = _SEARCHRESULTS;
	
	$kondisi="
	( 
	nama_pemesan LIKE '%$keyword%' OR
	nama_panggilan LIKE '%$keyword%' OR
	wa_pemesan LIKE '%$keyword%' OR
	email_pemesan LIKE '%$keyword%'
	) ";
	$order_by='ORDER BY tanggal_order DESC';
	$sql = "SELECT id FROM qmf_relaxation_order WHERE $kondisi ";

	$result = $mysql->query($sql);
	$total_records = $mysql->num_rows($result);
	$pages = ceil($total_records/$max_page_list);
	
	if ($mysql->num_rows($result) > 0) {
		$start = $screen * $max_page_list;
		$sql = "SELECT id,kode,tanggal_order,nama_pemesan,nama_panggilan,jenis_kelamin,wa_pemesan,email_pemesan,total,status_order FROM qmf_relaxation_order WHERE $kondisi $order_by LIMIT $start, $max_page_list";
		$result = $mysql->query($sql);

		if ($pages>1) $adminpagination = pagination($namamodul,$screen,$prmtr='keyword='.$keyword.'&action=search');
		$admincontent .= "<table class=\"list\" border=\"1\" cellspacing=\"1\" cellpadding=\"6\">\n";
		$admincontent .= "<tr><th>Tanggal</th>";
		$admincontent .= "<th>Kode</th>";
		$admincontent .= "<th>Nama</th>";
		$admincontent .= "<th>Panggilan</th>";
		$admincontent .= "<th>Jenis Kelamin</th>";
		$admincontent .= "<th>WA</th>";
		$admincontent .= "<th>Email</th>";
		$admincontent .= "<th>Nominal</th>";
		$admincontent .= "<th>Status</th>";
		$admincontent .= "<th>"._EDIT."</th><th>"._DEL."</th></tr>\n";
		while (list($id, $kode,$tanggal_order,$nama_pemesan,$nama_panggilan,$jenis_kelamin,$wa_pemesan,$email_pemesan,$total, $status_order) = $mysql->fetch_row($result)) {
			$admincontent .= "<tr ".($status_order==1?"style='background-color:lightgreen;'":"").">\n";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$tanggal_order</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$kode</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$nama_pemesan</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$nama_panggilan</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$jenis_kelamin</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$wa_pemesan</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$email_pemesan</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">$total</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"").">".($status_order==1?"Sudah Bayar":"Belum Bayar")."</td>";
			$admincontent .= "<td ".($status_order==1?"style='background-color:lightgreen;'":"")." align=\"center\"><a href=\"?p=product&action=modify&pid=$id\">";
			$admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
			$admincontent .= "<td  ".($status_order==1?"style='background-color:lightgreen;'":"")." align=\"center\"><a href=\"?p=product&action=remove&pid=$id\">";
			$admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
			$admincontent .= "</tr>\n";
		}
		$admincontent .= "</table>";
		
	} else {
		$admincontent = "<p>Tidak ada order</p>";
		
	}

}


if ($action == "tambahdata") {
	$judul = checkrequired($judul, _TITLE);
	$url = seo_friendly_url($url, 'page');
	if ($url == '') $url = seo_friendly_url($judul, 'page');
	$sql = "INSERT INTO page (judul, isi, url) VALUES ('$judul', '$isi', '$url')";
	$result = $mysql->query($sql);
	if ($result) {
		pesan(_SUCCESS,_DBSUCCESS,"?p=page&r=$random");
	} else {
		pesan(_ERROR,_DBERROR);
	}
}

// MODIFY PAGE
if ($action == "modify") {
    $admintitle = "Edit Order";
	$sql = "SELECT id,tanggal_order, kode,nama_pemesan,nama_panggilan,jenis_kelamin,wa_pemesan,email_pemesan,status_order FROM qmf_relaxation_order WHERE id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) == "0") {
		pesan(_ERROR,_NOPAGE);
	} else {
		list($id,$tanggal_order, $kode, $nama_pemesan, $nama_panggilan,$jenis_kelamin, $wa_pemesan, $email_pemesan, $status_order) = $mysql->fetch_row($result);
		$publishstatus = ($publish==1) ? 'checked' : '';
		$catselect = adminselectcategories('linkcat',$cat_id);
		$admincontent .= '
		<form method="POST" action="'.$thisfile.'">
			<input type="hidden" name="action" value="ubahdata">
			<input type="hidden" name="pid" value="'.$pid.'">
			<table border="0" cellspacing="1" cellpadding="3">
			  <tr>
				<td align="right">Tanggal:</td>
				<td>'.$tanggal_order.'</td>
			  </tr>
			  <tr>
				<td align="right">Kode:</td>
				<td>'.$kode.'</td>
			  </tr>
			  <tr>
				<td align="right">Nama:</td>
				<td><input type="text" name="nama_pemesan" value="'.$nama_pemesan.'" size="40">
			  </tr>
			  <tr>
				<td align="right">Panggilan:</td>
				<td><input type="text" name="nama_panggilan" value="'.$nama_panggilan.'" size="40"></td>
			  </tr>
			  <tr>
				<td align="right">Jenis Kelamin:</td>
				<td>
					<label for="laki-laki"><input type="radio" id="laki-laki" name="jenis_kelamin" value="Laki-laki" '.($jenis_kelamin=='Laki-laki'?'checked="checked"':'').'>Laki-Laki</label>
					<label for="perempuan"><input type="radio" id="perempuan" name="jenis_kelamin" value="Perempuan" '.($jenis_kelamin=='Perempuan'?'checked="checked"':'').'>Perempuan</label>
				</td>
			  </tr>
			  <tr>
				<td align="right">WA:</td>
				<td><input type="text" name="wa_pemesan" value="'.$wa_pemesan.'" size="40"></td>
			  </tr>
			  <tr>
				<td align="right">Email:</td>
				<td><input type="text" name="email_pemesan" value="'.$email_pemesan.'" size="40"></td>
			  </tr>
			 
			  <tr>
				<td align="right" valign="top">Status Order:</td>
				<td>
					<label for="pending"><input type="radio" id="pending" name="status_order" value="0" '.($status_order=='0'?'checked="checked"':'').'>BELUM BAYAR</label><br/>
					<label for="sudah_bayar"><input type="radio" id="sudah_bayar" name="status_order" value="1" '.($status_order=='1'?'checked="checked"':'').'>SUDAH BAYAR(Email berisi link download akan dikirimkan)</label><br/>
			  </tr>
			 
			  <tr>
				<td align="right" valign="top">&nbsp;</td>
				<td><input type="submit" name="submit" value="'._EDIT.'"></td>
			  </tr>
			</table>
		</form>
		';
	}
}

if ($action == "ubahdata") {
	$nama_pemesan = fiestolaundry($_POST['nama_pemesan'],200);
	$nama_panggilan = fiestolaundry($_POST['nama_panggilan'],200);
	$jenis_kelamin = fiestolaundry($_POST['jenis_kelamin'],40);
	$email_pemesan = fiestolaundry($_POST['email_pemesan'],200);
	$status_order = fiestolaundry($_POST['status_order'],1);

	$nama_pemesan = checkrequired($nama_pemesan,"Nama Pemesan");
	$wa_pemesan = checkrequired($wa_pemesan,"WA Pemesan");
	$email_pemesan = checkrequired($email_pemesan,"Email Pemesan");
	$tambahan="";
	if($status_order==1) {
		$tambahan=",status_time='$waktu_sekarang'";
	}
	//, wa_pemesan='$wa_pemesan',
	$sql = "UPDATE qmf_relaxation_order SET nama_pemesan='$nama_pemesan',jenis_kelamin='$jenis_kelamin', email_pemesan='$email_pemesan',status_order='$status_order' $tambahan WHERE id='$pid'"; 
	$result = $mysql->query($sql);
	if ($result) {
		if($status_order==1) {
		$sql = "SELECT kode FROM qmf_relaxation_order WHERE id='$pid'";
		$result = $mysql->query($sql);
		list($kode) = $mysql->fetch_row($result);
		
		$link_download="$cfg_app_url/product/download/$kode";
			
		$replace=array(
			'#id_order'=>$kode,
			'#link_download'=>$link_download,
			'#nama_pemesan'=>$nama_pemesan,
			'#waktu_sekarang'=>$waktu_sekarang,
			'#bapak_ibu'=>($jenis_kelamin=='Laki-laki'?'Bapak':'Ibu'),
			'#wa_pemesan'=>$wa_pemesan,
			'#email_pemesan'=>$email_pemesan
			);	
		/*REGISTER MEMBER*/
		//cek apakah email sudah terpakai
		$sql = "SELECT user_id FROM $tabelwebmember WHERE user_email='$email_pemesan'"; 
		
		$result = $mysql->query($sql);
		if ($result and $mysql->num_rows($result)<=0) { 
			$newpass = rand(0,9).time().rand(0,9);
			$gender=$jenis_kelamin=='Perempuan'?'0':'1';
			$cookedpass=fiestopass($newpass);
			$sql="INSERT INTO webmember SET username='$email_pemesan', 
				user_password='$cookedpass', fullname='$nama_pemesan', initial='$nama_panggilan',gender='$gender', level='1',
				activity='1', 
				cellphone1='$wa_pemesan', 
				user_email='$email_pemesan',
				user_regdate='$waktu_sekarang'";
			$result = $mysql->query($sql);
			if($result) {
				$replace['#info_login']='
				Berikut adalah info login anda:<br/>
				Username : '.$email_pemesan.' <br/>
				Password : '.$newpass.' <br/>
				Silahkan login di '.$cfg_app_url.'/webmember
				';
			
			}
		}
			
		/*END REGISTER MEMBER*/	
		/*KIRIM WA*/		
			if(count($replace)>0){
				foreach($replace as $i =>$v){
					$qmf_relaxation_wa_link=str_replace($i,$v,$qmf_relaxation_wa_link);
				}
			}
		//END BUAT INFO NOTA//
		
	
		
		//send wa to buyer
		//$log=send_wa($wa_pemesan,$qmf_relaxation_wa_link);
		if($_SESSION['testing']==1) {
		//	$log=send_wa($wa_pemesan,$qmf_relaxation_wa_link);
		}
		$update_status=$mysql->query("UPDATE qmf_relaxation_order SET wa_send_time='".date("Y-m-d H:i:s")."', wa_log_message='".$log."' WHERE  id=$id_order ");
		/*END KIRIM WA*/
		
		/*KIRIM EMAIL*/
		
		$sql = "SELECT value FROM contact WHERE name='umbalemail'";
		$result = $mysql->query($sql);
		list($umbalemail) = $mysql->fetch_row($result);
		list($email_admin) = explode(",",$umbalemail);
		
	
		if(count($replace)>0){
			foreach($replace as $i =>$v){
				$qmf_relaxation_email_link=str_replace($i,$v,$qmf_relaxation_email_link);
			}
		}
		
		
		//email to buyer
		$sukses=fiestophpmailer($email_pemesan,"Link Download #$kode $nama_pemesan ",'',$smtpuser,$nama_pemesan,$email_admin,$qmf_relaxation_email_link);
		$update_status=$mysql->query("UPDATE qmf_relaxation_order SET email_send_time='".date("Y-m-d H:i:s")."', email_send_status='".$sukses."' WHERE  id=$id_order ");
		
		/*KIRIM EMAIL*/	
		}
		pesan(_SUCCESS,_DBSUCCESS,"?p=product&r=$random");
	} else {
		pesan(_ERROR,_DBERROR);
	}
}

// DELETE LINK
if ($action == "remove" || $action == "delete") { //errhandler utk antisipasi pengetikan URL langsung di browser
	$sql = "SELECT id FROM qmf_relaxation_order WHERE id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) == "0") {
		pesan(_ERROR,_NOPAGE);
	} else {
		if ($action == "remove") {
			$admintitle = 'Hapus Order';
			$admincontent = _PROMPTDEL;
			$admincontent .= "<a href=\"?p=product&action=delete&pid=$pid\">"._YES."</a> <a href=\"javascript:history.go(-1)\">"._NO."</a></p>";
		} else {
			$sql = "DELETE FROM qmf_relaxation_order WHERE id='$pid'";
			$result = $mysql->query($sql);
			if ($result) {
				pesan(_SUCCESS,_DBSUCCESS,"?p=product&r=$random");
			} else {
				pesan(_ERROR,_DBERROR);
			}
		}
	}
}
if ($specialadmin=='') $specialadmin = "<a href=\"?p=product\">"._BACK."</a>";

?>
