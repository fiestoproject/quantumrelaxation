<?php
if (!$isloadfromindex) {
	include ("../../kelola/urasi.php");
	include ("../../kelola/fungsi.php");
	include ("../../kelola/lang/$lang/definisi.php");
	pesan(_ERROR,_NORIGHT);
}

$sort = "title";

$keyword=fiestolaundry($_GET['keyword'],100);
$screen=fiestolaundry($_GET['screen'],11);
$pid=fiestolaundry($_REQUEST['pid'],11);

$filetitle = fiestolaundry($_POST['filetitle'],200);
$publish = fiestolaundry($_POST['publish'],1);
$cat_id = fiestolaundry($_REQUEST['cat_id'],11);
$action = fiestolaundry($_REQUEST['action'],20);
$nama = fiestolaundry($_POST['nama'],255);
$urutan = fiestolaundry($_POST['urutan'],11);

if ($action=='') {
	header("location:?p=qmf_file&action=viewcat&cat_id=1");
	exit();
	$admincontent = adminlistcategories('qmf_filecat','qmf_file');
	$sqlcat = "SELECT id FROM qmf_filecat ";
	$resultcat = $mysql->query($sqlcat);
	
	//$specialadmin = "<a href=\"?p=qmf_file&action=catnew\">"._ADDCAT."</a> ";
	if($mysql->num_rows($resultcat)>0)
	{	$specialadmin .= "<a href=\"?p=qmf_file&action=add\">"._ADDFILE."</a>";
	}
}

if ($action == "viewcat") {

	$sql = "SELECT id FROM qmf_filedata WHERE cat_id='$cat_id'";
	$result = $mysql->query($sql);
	$total_records = $mysql->num_rows($result);
	$pages = ceil($total_records/$max_page_list);

	if ($mysql->num_rows($result) == "0") {
		$admincontent .= _NOFILE;
	} else {
		$sql = "SELECT nama FROM qmf_filecat WHERE id='$cat_id'";
		$result = $mysql->query($sql);
		list($catname) = $mysql->fetch_row($result);
		$admintitle = $catname;
		$start = $screen * $max_page_list;
		$sql = "SELECT id, cat_id, filename, filesize, title, clickctr, urutan FROM qmf_filedata WHERE cat_id='$cat_id' ORDER BY $sort LIMIT $start, $max_page_list";
		$result = $mysql->query($sql);

		if ($pages>1) $adminpagination = pagination($namamodul,$screen);
		$admincontent .= "<table class=\"list\" border=\"1\" cellspacing=\"1\" cellpadding=\"6\">\n";
		$admincontent .= "<tr>";
		//$admincontent .= "<th>"._ORDER."</th>";
		$admincontent .= "<th>"._FILENAME."</th><th>"._FILESIZE."</th><th>"._FILETITLE."</th><th>"._FILEVISITED."</th>";
		$admincontent .= "<th>"._EDIT."</th><th>"._DEL."</th></tr>\n";
		$i = $start+1;
		while (list($id, $cat_id, $filename, $filesize, $title, $clickctr, $urutan) = $mysql->fetch_row($result)) {
			$admincontent .= "<tr>\n";
			//$admincontent .= "<td>$i</td>";
			$admincontent .= "<td>$filename</td><td align=\"right\">".convertbyte($filesize)."</td><td>$title</td><td>$clickctr</td>";
			$admincontent .= "<td align=\"center\"><a href=\"?p=qmf_file&action=modify&pid=$id\">";
			$admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
			$admincontent .= "<td align=\"center\"><a href=\"?p=qmf_file&action=remove&pid=$id\">";
			$admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
			$admincontent .= "</tr>\n";
			$i++;
		}
		$admincontent .= "</table>";
	}

	$specialadmin .= "<a href=\"?p=qmf_file&action=add\">"._ADDFILE."</a> <a href=\"?p=qmf_file\">"._BACK."</a>";
}

if ($action == 'search') {
    $admintitle = _SEARCHRESULTS;

	$sql = "SELECT id FROM qmf_filedata WHERE filename LIKE '%$keyword%' OR title LIKE '%$keyword%'";
	$result = $mysql->query($sql);
	$total_records = $mysql->num_rows($result);
	$pages = ceil($total_records/$max_page_list);

	if ($mysql->num_rows($result) > 0) {
		$admintitle = _SEARCHRESULTS;
		$sql = "SELECT nama FROM qmf_filecat WHERE id='$cat_id'";
		$result = $mysql->query($sql);
		list($catname) = $mysql->fetch_row($result);
		$admintitle = $catname;
		$start = $screen * $max_page_list;
		$sql = "SELECT id, cat_id, filename, filesize, title, clickctr, urutan FROM qmf_filedata WHERE filename LIKE '%$keyword%' OR title LIKE '%$keyword%' ORDER BY $sort LIMIT $start, $max_page_list";
		$result = $mysql->query($sql);

		if ($pages>1) $adminpagination = pagination($namamodul,$screen,"action=search&keyword=$keyword");
		$admincontent .= "<table class=\"list\" border=\"1\" cellspacing=\"1\" cellpadding=\"6\">\n";
		$admincontent .= "<tr>";
		//$admincontent .= "<th>"._ORDER."</th>";
		$admincontent .= "<th>"._FILENAME."</th><th>"._FILESIZE."</th><th>"._FILETITLE."</th><th>"._FILEVISITED."</th>";
		$admincontent .= "<th>"._EDIT."</th><th>"._DEL."</th></tr>\n";
		$i = $start+1;
		while (list($id, $cat_id, $filename, $filesize, $title, $clickctr, $urutan) = $mysql->fetch_row($result)) {
			$admincontent .= "<tr>\n";
			//$admincontent .= "<td>$i</td>";
			$admincontent .= "<td>$filename</td><td align=\"right\">".convertbyte($filesize)."</td><td>$title</td><td>$clickctr</td>";
			$admincontent .= "<td align=\"center\"><a href=\"?p=qmf_file&action=modify&pid=$id\">";
			$admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
			$admincontent .= "<td align=\"center\"><a href=\"?p=qmf_file&action=remove&pid=$id\">";
			$admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
			$admincontent .= "</tr>\n";
		}
		$admincontent .= "</table>";
	} else {
		$admincontent .= "<p>"._NOSEARCHRESULTS."</p>";
	}
}

if ($action == "add") {
	$catselect = adminselectcategories('qmf_filecat');
	$admintitle = _ADDFILE;
    $admincontent .= '
	<form method="POST" action="'.$thisfile.'" enctype="multipart/form-data">
	  <input type="hidden" name="action" value="tambahdata">
	    <table border="0" cellspacing="1" cellpadding="3">
		  <tr style="display:none;">
			<td align="right">'._CATEGORY.':</td>
			<td>'.$catselect.'</td>
		  </tr>
	      <tr>
	        <td align="right">'._FILENAME.':</td>
	        <td><input type="file" name="filename"></td>
	      </tr>
	      <tr>
	        <td align="right">'._FILETITLE.':</td>
	        <td><input type="text" name="filetitle" size="40">
	      </tr>';
	/*$admincontent .= '<tr>
			<td align="right" valign="top">'._ORDER.':</td>
			<td><input type="text" name="urutan" size="2" /></td>
		  </tr>';*/
	$admincontent .= '<tr>
	        <td align="right" valign="top">&nbsp;</td>
	        <td><input type="submit" name="submit" value="'._ADD.'"></td>
	      </tr>
	    </table>
	</form>
    ';
}

if ($action == "tambahdata") {
	$sqlcat = "SELECT id FROM qmf_filecat WHERE id='$cat_id' ";
	$resultcat = $mysql->query($sqlcat);
	if($mysql->num_rows($resultcat)==0)
	{	pesan(_ERROR,_CATEGORYERROR);
	}
	
	$namafile = $_FILES['filename']['name'];
	$ukuranfile = $_FILES['filename']['size'];
	$hasilupload = fiestoupload('filename',$cfg_file_path,'',$maxfilesize,$allowedfiletypes);
	if ($hasilupload==_SUCCESS) {
		$sql = "INSERT INTO qmf_filedata (cat_id, filename, filesize, title, urutan) VALUES ('$cat_id', '$namafile', '$ukuranfile', '$filetitle', '$urutan')";
		$result = $mysql->query($sql);
		if ($result) {
			pesan(_SUCCESS,_DBSUCCESS,"?p=qmf_file&action=viewcat&cat_id=$cat_id&r=$random");
		} else {
			pesan(_ERROR,_DBERROR);
		}
	} else {
		pesan(_ERROR,$hasilupload);
	}
}

if ($action == "modify") {
	$admintitle = _EDITFILE;
	
	$sql = "SELECT  id, cat_id, filename, filesize, title, clickctr, urutan FROM qmf_filedata WHERE id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) == "0") {
		pesan(_ERROR,_NOFILE);
	} else {
		
		list($id, $cat_id, $filename, $filesize, $title, $clickctr, $urutan) = $mysql->fetch_row($result);
		$publishstatus = ($publish==1) ? 'checked' : '';
		$catselect = adminselectcategories('qmf_filecat',$cat_id);
		$admincontent .= '
			<form method="POST" action="'.$thisfile.'">
				<input type="hidden" name="action" value="ubahdata">
				<input type="hidden" name="pid" value="'.$pid.'">
				<table border="0" cellspacing="1" cellpadding="3">
				  <tr>
					<td align="right">'._CATEGORY.':</td>
					<td>'.$catselect.'</td>
				  </tr>
				  <tr>
					<td align="right">'._FILENAME.':</td>
					<td>'.$filename.'</td>
				  </tr>
				  <tr>
					<td align="right">'._FILESIZE.':</td>
					<td>'.convertbyte($filesize).'</td>
				  </tr>
				  <tr>
					<td align="right">'._FILETITLE.':</td>
					<td><input type="text" name="filetitle" value="'.$title.'" size="40">
				  </tr>';
		/*$admincontent .= '<tr>
					<td align="right" valign="top">'._ORDER.':</td>
					<td><input type="text" name="urutan" value="'.$urutan.'" size="2" /></td>
				  </tr>';*/
		$admincontent .= '<tr>
					<td align="right" valign="top">&nbsp;</td>
					<td><input type="submit" name="submit" value="'._EDIT.'"></td>
				  </tr>
				</table>
			</form>';
		}
}

if ($action == "ubahdata") {
	$sqlcat = "SELECT id FROM qmf_filecat WHERE id='$cat_id' ";
	$resultcat = $mysql->query($sqlcat);
	if($mysql->num_rows($resultcat)==0)
	{	pesan(_ERROR,_CATEGORYERROR);
	}
	$sql = "UPDATE qmf_filedata SET cat_id='$cat_id', title='".$filetitle."', urutan='$urutan' WHERE id='$pid'"; 
	$result = $mysql->query($sql);
	if ($result) {
		pesan(_SUCCESS,_DBSUCCESS,"?p=qmf_file&action=viewcat&cat_id=$cat_id&r=$random");
	} else {
		pesan(_ERROR,_DBERROR);
	}
}

if ($action == "remove" || $action == "delete") { //errhandler utk antisipasi pengetikan URL langsung di browser
	$admintitle = _DELFILE;
	$sql = "SELECT id, cat_id, filename FROM qmf_filedata WHERE id = '$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) == "0") {
		pesan(_ERROR,_NOFILE);
	} else {
		list($id,$cat_id,$filename) = $mysql->fetch_row($result);
		if ($action == "remove") {
			$admincontent .= _PROMPTDEL;
			$admincontent .= "<a href=\"?p=qmf_file&action=delete&pid=$pid\">"._YES."</a> <a href=\"javascript:history.go(-1)\">"._NO."</a></p>";
		} else {
			$sql = "DELETE FROM qmf_filedata WHERE id='$pid'";
			$result = $mysql->query($sql);
			if ($result) {
				pesan(_SUCCESS,_DBSUCCESS,"?p=qmf_file&action=viewcat&cat_id=$cat_id&r=$random");
			} else {
				pesan(_ERROR,_DBERROR);
			}
			unlink($cfg_file_path.'/'.$filename);
		}
	}
}



if ($action == "catnew" || $action == "catedit") {
		$admincontent .= "<form method=\"POST\" action=\"$thisfile\">";
		if ($action == "catnew") {
			$admintitle = _ADDCAT;
			$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"baru\" />";
		}
		if ($action == "catedit") {
			if (empty($cat_id)) pesan(_ERROR,_NOCAT);
			$admintitle = _EDITCAT;
			$sql =	"SELECT nama,urutan FROM qmf_filecat WHERE id=$cat_id ORDER BY urutan";
			$result = $mysql->query($sql);
			if ($mysql->num_rows($result)==0) pesan(_ERROR,_NOCAT);
			$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"ubah\" />";
			$admincontent .= "<input type=\"hidden\" name=\"cat_id\" value=\"$cat_id\" />";
			list($cat_name,$cat_order) = $mysql->fetch_row($result);
		}
		$admincontent .= "<table border=\"0\"><tr><td>"._CATNAME.":</td><td><input type=\"text\" name=\"nama\" value=\"$cat_name\" /></td></tr>\r\n";
		$admincontent .= "<tr><td>"._ORDER.":</td><td>";
		$admincontent .= "<div id=\"urutancontent\">";
		$admincontent .= createurutan("qmf_filecat", "", $cat_id);
		$admincontent .= "</div>";
		//$admincontent .= "<input type=\"text\" size=\"2\" name=\"urutan\" value=\"$cat_order\" />";
		$admincontent .= "</td></tr>\r\n";
		$admincontent .= "<tr><td>&nbsp;</td><td><input type=\"submit\" name=\"submit\" value=\""._SAVE."\" /></td></tr>\r\n";
		$admincontent .= "</table>\r\n";
		$admincontent .= "</form>\r\n";
}

if ($action == "catdel") {
		if (empty($cat_id)) pesan(_ERROR,_NOCAT);
		$admintitle = _DELCAT;
		$sql =	"SELECT nama,urutan FROM qmf_filecat WHERE id=$cat_id ORDER BY urutan";
		$result = $mysql->query($sql);
		if ($mysql->num_rows($result)==0) pesan(_ERROR,_NOCAT);
		$admincontent  = _PROMPTDELCAT;
		$admincontent .= "<a href=\"?p=qmf_file&action=hapus&cat_id=$cat_id\">"._YES."</a> &nbsp;<a href=\"javascript:history.go(-1)\">"._NO."</a>";
}

if ($action == "baru") {
		$nama = checkrequired($nama, _CATNAME);
		if (!preg_match('/[0-9]*/',$urutan)) $urutan=1;
		$mx = getMaxNumber('qmf_filecat', 'urutan')+2;
		$sql = "INSERT qmf_filecat (nama, urutan) values ('$nama','$mx')";
		$result = $mysql->query($sql);
		$mid = mysql_insert_id();
		$result = urutkan('qmf_filecat', $urutan, "", $mid, "");
		if ($result) {
			pesan(_SUCCESS,_DBSUCCESS,"?p=qmf_file&r=$random");
		} else {
			pesan(_ERROR,_DBERROR);
		}
}

if ($action == "ubah") {
		$nama = checkrequired($nama, _CATNAME);
		$cat_id = checkrequired($cat_id, _CATID);
		if (!preg_match('/[0-9]*/',$urutan)) $urutan=1;
		
		$kondisiprev = "";
		$kondisi = "";
		
		$sql = "UPDATE qmf_filecat set nama='$nama' WHERE id='$cat_id'";
		$result = $mysql->query($sql);
		
		$result = urutkan('qmf_filecat', $urutan, $kondisi, $cat_id, $kondisiprev);
		if($kondisi!=$kondisiprev)
		{	urutkansetelahhapus('qmf_filecat', $kondisiprev);
		}
		
		if ($result) {
			pesan(_SUCCESS,_DBSUCCESS,"?p=qmf_file&r=$random");
		} else {
			pesan(_ERROR,_DBERROR);
		}
}

if ($action == "hapus") 
{
	if ($cat_id=='') pesan(_ERROR,_NOFILE);
		
	$sql = "SELECT * FROM qmf_filecat ";
	$result = $mysql->query($sql);
	
	if ($mysql->num_rows($result) <= 1) 
	{	pesan(_ERROR,_1CAT,"?p=qmf_file&r=$random");
	}
	
	$sql = "DELETE FROM qmf_filecat WHERE id='$cat_id'";
	if (!$mysql->query($sql)) 
	{	pesan(_ERROR,_DBERROR);
	}
	else
	{	urutkansetelahhapus("qmf_filecat", "");
	}
	$sql = "DELETE FROM qmf_filedata WHERE cat_id='$cat_id'";
	if ($mysql->query($sql)) 
	{	pesan(_SUCCESS,_DBSUCCESS,"?p=qmf_file&r=$random");
	} 
	else 
	{	pesan(_ERROR,_DBERROR);
	}
			
}
if ($specialadmin=='') $specialadmin = "<a href=\"?p=qmf_file\">"._BACK."</a>";

?>
