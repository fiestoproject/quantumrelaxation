<?php

if (!$isloadfromindex) {
    include ("../../kelola/urasi.php");
    include ("../../kelola/fungsi.php");
    include ("../../kelola/lang/$lang/definisi.php");
    pesan(_ERROR, _NORIGHT);
}

$keyword = fiestolaundry($_GET['keyword'], 100);
$screen = fiestolaundry($_GET['screen'], 11);
$pid = fiestolaundry($_REQUEST['pid'], 11);
$action = fiestolaundry($_REQUEST['action'], 10);
$t_id = $_POST['t_id'];
$templatename = $_POST['templatename'];
$social_media = array(8,9,10,11,12,13);
$tinymce = array(14,17,18,19,20);
$exclude = "(1,2,3,4)";

$modulename = $_GET['p'];

if ($action == "save") {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
		$temp = array();
		for($i = 1; $i <= 3; $i++) $temp[] = "template$i";
		$template = join(',',$temp);
        if ($notificationbuilder != "") {
            $action = createmessage($notificationbuilder, _ERROR, "error", "add");
        } else 
		{
            $sql = "INSERT INTO template_option (id,judul, $template) VALUES ('$idpage','$judul','$templaterow1','$templaterow2','$templaterow3','$templaterow4','$templaterow5','$templaterow6','$templaterow7')";
            $result = $mysql->query($sql);
			if ($result) {
				$action = createmessage(_ADDSUCCESS, _SUCCESS, "success", "");
			} else {
				$action = createmessage(_DBERROR, _ERROR, "error", "add");
			}
        }
    } else {
        $action = "add";
    }
}
if ($action == "add") {
    $admintitle = _ADDPAGE;
    $admincontent .= '
	<form class="form-horizontal" method="POST" action="' . $thisfile . '">
	  <input type="hidden" name="action" value="save">';
	for($i = 1; $i <= 3; $i++) {
		$admincontent .= '	      
		  <div class="control-group">
	        <label class="control-label">' . _PAGECONTENT . $i . '</label>
	        <div class="controls"><textarea name="template'.$i.'" rows="20" cols="60" class="usetiny"></textarea></div>
	      </div>';
	}
	$admincontent .= '
	      <div class="control-group">
	        <div class="controls"><input type="submit" name="submit" class="buton"  value="' . _SAVE . '">
                     <input type="submit" name="back" class="buton" value="' . _BACK . '">
                         </div>
	      </div>
	</form>
    ';
}

if ($action == "update") {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        if ($notificationbuilder != "") {
            $action = createmessage($notificationbuilder, _ERROR, "error", "modify");
        } else {
			$ctotal = count($templatename);
			if ($ctotal > 0) {
				for($i = 0; $i < $ctotal; $i++) {
					if (!$mysql->query("UPDATE template_option SET template='".mysql_real_escape_string($templatename[$i])."' WHERE id='".$t_id[$i]."'")) $action = createmessage(_DBERROR, _ERROR, "error", "modify"); //else $action = createmessage(_EDITSUCCESS, _SUCCESS, "success", "");
				}
			}
			
			$sql = $mysql->query("SELECT id, judul, maxdimension FROM template_option WHERE id NOT IN $exclude");
			while($row = $mysql->fetch_assoc($sql)) {
				$basename = str_replace(' ','',strtolower($row['judul']));
				$url = fiestolaundry($_POST['url_'.$basename],255);
				$title = fiestolaundry($_POST['title_'.$basename],100);
				if ($_FILES[$basename]['error'] != UPLOAD_ERR_NO_FILE) {
					$hasil_upload = fiestoupload($basename, "$cfg_decoration_path", $basename, $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png,ico");
					if ($hasil_upload != _SUCCESS) {
						$action = createmessage($hasilupload, _ERROR, "error", "");
					} else {
						$str_path = pathinfo($_FILES[$basename]['name']);
						$filename = $str_path['basename'];
						$extension = $str_path['extension'];
						
						$modifiedfilename = "$basename.$extension";
						list($aturan, $maxwidth, $maxheight) = explode(';', $row['maxdimension']);
						switch($aturan) {
							case 'fixed':
								break;
							case 'max':
								if(file_exists("$cfg_decoration_path/$basename.$extension")) {
									list($filewidth, $fileheight, $filetype, $fileattr) = getimagesize("$cfg_decoration_path/$basename.$extension");
										if ($filewidth > $maxwidth || $fileheight > $maxheight) {
										$hasilresize = fiestoresize("$cfg_decoration_path/$basename.$extension", "$cfg_decoration_path/$basename.$extension", 'b', $maxwidth, $maxheight);
										if ($hasilresize != _SUCCESS) {
											$action = createmessage($hasilresize, _ERROR, "error", "");
										}
									}
								}
								break;
						}
			
						if (!$mysql->query("UPDATE template_option SET template='$modifiedfilename', url='$url', title='$title' WHERE id='{$row['id']}'")) createmessage(_DBERROR, _ERROR, "error", "");
					}
				} else {
					if (!$mysql->query("UPDATE template_option SET url='$url', title='$title' WHERE id='{$row['id']}'")) createmessage(_DBERROR, _ERROR, "error", "");
				}
			}
			$action = createmessage(_DBSUCCESS, _SUCCESS, "success", "");
        }
    } else {
        $action = "modify";
    }
}

if ($action == "modify" || $action == "" ) {
    $sql = "SELECT id, judul, template, maxdimension, url, title FROM template_option WHERE id NOT IN $exclude ORDER BY id ";

    $result = $mysql->query($sql);
    if ($mysql->num_rows($result) == "0") {
        $action = createmessage(_NOPAGE, _INFO, "info", "");
    } else {
        $admincontent .= '
		<form class="form-horizontal" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
			<input type="hidden" name="action" value="update">
			<input type="hidden" name="pid" value="' . $pid . '">';
			while(list($id, $judul, $template, $maxdimension, $url, $title) = mysql_fetch_row($result)) {
				$basename = str_replace(' ','',strtolower($judul));
				
				if (in_array($id, $social_media)) {
					$admincontent .= '
						 <div class="control-group">
							<label class="control-label">' . $judul . '</label>
							<div class="controls">
								<input type="hidden" name="t_id[]" value='.$id.'>
								<input type="text" name="templatename[]" value="'.$template.'">
							</div>
						 </div>';					
				} else if (in_array($id, $tinymce)) {
					$admincontent .= '
						 <div class="control-group">
							<label class="control-label">' . $judul . '</label>
							<div class="controls">
								<input type="hidden" name="t_id[]" value='.$id.'>
								<textarea name="templatename[]" rows="20" cols="60" class="usetiny">' . $template . '</textarea>
							</div>
						 </div>';
				} else {
					$images = ($template != '' && file_exists("$cfg_decoration_path/$template")) ? '<img src="'.$cfg_decoration_url.'/'.$template.'" style="margin-bottom:10px">' : '';
					$temp = explode(';', $maxdimension);
					$admincontent .= '
						 <div class="control-group">
							<label class="control-label">' . $judul . '</label>
							<div class="controls">
								'.$images.'
								<div data-provides="fileupload" class="fileupload fileupload-new">
									
									<div class="input-append">
										<div class="uneditable-input span2">
											<i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span>
										</div>
										<span class="btn btn-file">
											<span class="fileupload-new">Cari File</span>
											<span class="fileupload-exists">Ubah</span>
											<input type="file" name="'.$basename.'" size="35">
										</span>
										<!--<a data-dismiss="fileupload" class="btn" fileupload-exists="" href="#">Remove</a>-->
									</div>
									<div class="dimension">
										Max dimension: '.$temp[1].' x '.$temp[2].'
									</div>
									<div class="dimension">
										<input type="text" name="url_'.$basename.'" value="'.$url.'" placeholder="Url">
									</div>
									<div class="dimension">
										<input type="text" name="title_'.$basename.'" value="'.$title.'" placeholder="Title">
									</div>
								</div>
							</div>
						 </div>';
				}
			 }
			 $admincontent .= '
			 <div class="control-group">
				<div class="controls">
					<input type="submit" name="submit" class="buton"  value="' . _SAVE . '">
					<input type="submit" name="back" class="buton" value="' . _BACK . '">
				</div>
			</div>
		</form>
		';
    }
}
?>