<!DOCTYPE html>
<html lang="id">

<head>

	<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $config_site_metadescription;?>">
    <meta name="keyword" content="<?php echo $config_site_metakeyword;?>">
    <meta name="author" content="fiesto.com">
	<meta name="theme-color" content="#171717">
	<link href="<?php echo $favicon ?>" rel="SHORTCUT ICON" />
	
    <title><?php echo $config_site_titletag;?></title>
	<?php echo $style_css ?>
    <!-- Bootstrap Core CSS -->
    <link href="<<<TEMPLATE_URL>>>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
	<link type="text/css" rel="stylesheet" href="<<<TEMPLATE_URL>>>/font-awesome/css/font-awesome.min.css">
	<link type="text/css" href="<<<TEMPLATE_URL>>>/js/hoverzoom.css" rel="stylesheet">
	<!--<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>-->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,400italic,600italic,800' rel='stylesheet' type='text/css'>
    <link href="<<<TEMPLATE_URL>>>/css/color.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/styles.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/blog-post.css?ver=2.6" rel="stylesheet">
	<link href="<<<TEMPLATE_URL>>>/css/owl.carousel.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/owl.theme.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/owl.transitions.css" rel="stylesheet">
	<link href="<<<WEB_URL>>>/js/mp/magnific-popup.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style type="text/css">
	@media(min-width:768px){
		.dekstop-hidden{display:none !important}
	}
	@media(max-width:767px){
		.mobile-hidden{display:none !important}
	}
	<?php if(!$_GET['p']) {?>
	@media (max-width:767px){
	.navbar-header {
	}
	}
	<?php
		}
		else
		{
	?>
	@media (max-width:767px){
	.navbar-header {
	}
	}
	<?php }?>
	<?php if ($_GET['p'] == 'cart') : ?>
		.more {
			background: #b30f0f;
		}
		form#checkout-form img {
			max-width: none;
		}
		#checkout-form .biodatabawah {
			margin-top: 10px;
		}
	<?php endif; ?>
	<?php if ($_GET['p'] == 'order') : ?>
		.more {
			background: #b30f0f;
		}
	<?php endif; ?>
	<?php if ($_GET['p'] == 'catalog' && ($_GET['action'] == 'detail')) : ?>
	<?php endif; ?>
	</style>
	<script src="<<<TEMPLATE_URL>>>/js/jquery-1.7.2.min.js"></script>
	<script src="<<<WEB_URL>>>/js/mp/jquery.magnific-popup.min.js"></script>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse header1" role="navigation">
		<div class="container">
			<!--<div class="header-right">
				<ul class="topbar-nav login-menu" id="b-nav">
					<li class="public-li"> 
						<a class="cart" href="<?php echo $cfg_app_url;?>/cart">
							<span><?php echo fa_shopping_cart(); ?></span>
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</a>
					</li>
				</ul> 					
			</div>-->
			<div class="top-header">
				<div class="block-login">
					<ul class="topbar-nav login-menu list-inline" id="b-nav">
						<?php //echo membership_header_right(); ?>
					</ul>
				</div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="navbar-header">
				<a class="navbar-brand dekstop-hidden" href="<?php echo $cfg_app_url?>"><?php echo $dekorasi[4]; ?></a>       
				<a class="navbar-brand mobile-hidden" href="<?php echo $cfg_app_url?>"><?php echo $dekorasi[0]; ?></a>       
			</div>
			<div class="collapse navbar-collapse top-menu" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<?php echo $display_menu; ?>
					<li><a href="<?php echo $cfg_app_url;?>/product/login">Login</a></li>
					<li></li>
				</ul>
			</div><!--/.nav-collapse -->
			<div class="social-header">
				<?php echo headerLeft(); ?>
			</div>
		</div>
    </nav>
	   <!-- Page Content -->
	<?php if(!$_GET['p']) {?>
		<div class="slider">
			<!--<?php //echo $dekorasi[1] ?>-->
			<div id="slide-home" class="owl-carousel owl-theme">
				<div class="item">
					<div class="mobile-hidden"><?php echo $dekorasi[1] ?></div>
					<div class="dekstop-hidden"><?php echo $dekorasi[5] ?></div>
				</div>
				<div class="item">
					<div class="mobile-hidden"><?php echo $dekorasi[2] ?></div>
					<div class="dekstop-hidden"><?php echo $dekorasi[6] ?></div>
				</div>
				<div class="item">
					<div class="mobile-hidden"><?php echo $dekorasi[3] ?></div>
					<div class="dekstop-hidden"><?php echo $dekorasi[7] ?></div>
				</div>
			</div>
		</div>
		<?php echo getHeader(); ?>
    <?php } ?>
    <div id="content">
	<?php if(!$_GET['p']) {?>
	<?php //echo get_exclusive(); ?>
	<?php //echo get_popular(); ?>
	<?php echo $display_main_content_block;?>
	<?php 
		} 
		else 
		{	
	?>
		<?php echo $display_main_content_block;?>
	<?php } ?>
    </div>

	<!-- Footer -->

    <footer>
		<?php echo getfooterlink(); ?>
		<div class="copyright">
			<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?php echo $display_footer;?>
				</div>
			</div>
			</div>
		</div>
		<!-- /.row -->
	</footer>
	<div id="button-mobile">
		<div class="menu-button-mobile">
			<ul class="list-inline">
				<li><a href="<?php echo $cfg_app_url ?>"><span class="button-home-icon"></span></a></li>
				<!--<li><a href="mailto:<?php echo emailUrl(); ?>"><span class="button-menu-icon"></span></a></li>-->
				<li><a href="tel:<?php echo numberPhone(); ?>"><span class="button-phone-icon"></span></a></li>
				<li><a href="https://wa.me/<?php echo numberWA(); ?>"><span class="button-mail-icon"></span></a></li>
				<li><a href="<?php echo $cfg_app_url ?>/page/view/11_produk"><span class="button-product-icon"></span></a></li>
			</ul>
		</div>
	</div>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
    <!-- Bootstrap Core JavaScript -->
	<!--<script src="<<<TEMPLATE_URL>>>/js/jquery-1.7.2.min.js"></script>-->
    <script src="<<<TEMPLATE_URL>>>/js/bootstrap.min.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/owl.carousel.min.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="<<<TEMPLATE_URL>>>/js/jquery.selectbox-0.2.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.zoom.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/isotope.pkgd.min.js"></script>
	<?php
	echo $script_js;
	?>
	<script type="text/javascript">
	$(function() {
		$('.carousel-testimonials').owlCarousel({
		 
			autoPlay: 8000, //Set AutoPlay to 3 seconds
			items : 3,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			navigation : true
		 
		});
	});
	</script>	
	<script>
	
		$( document ).ready(function() {
			var sync1 = $("#sync1");
			  var sync2 = $("#sync2");

			  sync1.owlCarousel({
				singleItem : true,
				slideSpeed : 1000,
				navigation: true,
				pagination:false,
				afterAction : syncPosition,
				responsiveRefreshRate : 200,
			  });

			  sync2.owlCarousel({
				items : 9,
				itemsDesktop      : [1199,9],
				itemsDesktopSmall     : [979,9],
				itemsTablet       : [768,7],
				itemsMobile       : [479,7],
				pagination:false,
				responsiveRefreshRate : 100,
				afterInit : function(el){
				  el.find(".owl-item").eq(0).addClass("synced");
				}
			  });

			  function syncPosition(el){
				var current = this.currentItem;
				$("#sync2")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync2").data("owlCarousel") !== undefined){
				  center(current)
				}

			  }

			  $("#sync2").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			  });

			  function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

				var num = number;
				var found = false;
				for(var i in sync2visible){
				  if(num === sync2visible[i]){
					var found = true;
				  }
				}

				if(found===false){
				  if(num>sync2visible[sync2visible.length-1]){
					sync2.trigger("owl.goTo", num - sync2visible.length+2)
				  }else{
					if(num - 1 === -1){
					  num = 0;
					}
					sync2.trigger("owl.goTo", num);
				  }
				} else if(num === sync2visible[sync2visible.length-1]){
				  sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
				  sync2.trigger("owl.goTo", num-1)
				}
			  }
			$('#search-icon').on('click', function(){
				$('#searchproduct').toggleClass('active');
			});
			$('#close-search-icon').on('click', function(){
				$('#searchproduct').removeClass('active');
			});
			$("#popular-categories img").css("min-height",$("#popular-categories img").height());	
			$('.button-menu-fixed').on('click', function(e) {
			  $('#bs-example-navbar-collapse-3').toggleClass("show");
			  e.preventDefault();
			});
			$('.button-mobile-toggle').on('click', function(e) {
			  $('#button-mobile').toggleClass("nonactive");
			  $('body').toggleClass("mobile-display");
			  e.preventDefault();
			});
			$(".megamenu").each(function(index)
			{
				var find_th = $(this).find(".menu-li-standart");
				if(find_th.length > 0)
				{
					$(this).addClass("menu-standart");
				}
			});
		});
		$(function() {
			
			/*$(".image-product").css("padding-top",$(".caption").height());*/
			$('#myCarousel').carousel({
				interval: 10000
			})
			$('.fdi-Carousel .item').each(function () {
				var next = $(this).next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));

				if (next.next().length > 0) {
					next.next().children(':first-child').clone().appendTo($(this));
				}
				else {
					$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
				}
			});
			
			$("#product-carousel").owlCarousel({
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				items : 4,
				itemsDesktop : [1199,4],
				itemsDesktopSmall : [979,3],
				navigation : true
			 
			});
			
			$("#owl-demo1").owlCarousel({
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				items : 4,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,3],
				navigation : true
			 
			});	
			
			$("#slide-home").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,	transitionStyle : "fade",	
				autoPlay: 9000, //Set AutoPlay to 3 seconds
				paginationSpeed : 400,
				pagination : false,
				singleItem:true
			});
			
			$(".nav > li.dropdown > .dropdown-menu > li.dropdown > a").on("click",function(e){
				var current=$(this).next();
				var grandparent=$(this).parent().parent();
				if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
					$(this).toggleClass('right-caret left-caret');
				grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
				grandparent.find(".sub-menu:visible").not(current).show();
				current.toggle();
				e.stopPropagation();
			});
			$(".nav > li.dropdown > .dropdown-menu > li.dropdown > a:not(.trigger)").on("click",function(){
				var root=$(this).closest('.dropdown');
				root.find('.left-caret').toggleClass('right-caret left-caret');
				root.find('.sub-menu:visible').show();
			});	
			
			$('.zoom').zoom();
			$('#shop-slider').bxSlider({
				pagerCustom: '#shop-slider-pager'
			});
			$('#shop-slider1').bxSlider({
				pagerCustom: '#shop-slider-pager1'
			});	
			$('#shop-slider4').bxSlider({
				pagerCustom: '#shop-slider-pager4'
			});	
			$('#shop-slider7').bxSlider({
				pagerCustom: '#shop-slider-pager7'
			});	
			$('#shop-slider9').bxSlider({
				pagerCustom: '#shop-slider-pager9'
			});	
			
			$('.carousel-product').owlCarousel({
			 
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				 
				items : 5,
				itemsDesktop : [1199,5],
				itemsDesktopSmall : [980,4],
				itemsTablet: [768,3],
				itemsTabletSmall: false,
				itemsMobile : [479,2],
				singleItem : false,
				navigation : true
			 
			});	
			
			
			// init Isotope
			var $grid = $('.grid').isotope({
				itemSelector: '.element-item',
				layoutMode: 'fitRows',
				getSortData: {
					name: '.name',
					symbol: '.symbol',
					number: '.number parseInt',
					category: '[data-category]',
					weight: function( itemElem ) {
						var weight = $( itemElem ).find('.weight').text();
						return parseFloat( weight.replace( /[\(\)]/g, '') );
					}
				}
			});
			
			// filter functions
			var filterFns = {
				// show if number is greater than 50
				numberGreaterThan50: function() {
				var number = $(this).find('.number').text();
					return parseInt( number, 10 ) > 50;
				},
				// show if name ends with -ium
				ium: function() {
					var name = $(this).find('.name').text();
					return name.match( /ium$/ );
				}
			};

			// bind filter button click
			$('#filters').on( 'click', 'button', function() {
				var filterValue = $( this ).attr('data-filter');
				// use filterFn if matches value
				filterValue = filterFns[ filterValue ] || filterValue;
				$grid.isotope({ filter: filterValue });
			});			
			
			$('.popup-youtube').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,

				fixedContentPos: false
			});
			
		})

		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			// Avoid following the href location when clicking
			event.preventDefault(); 
			// Avoid having the menu to close when clicking
			event.stopPropagation(); 
			// If a menu is already open we close it
			//$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
			// opening the one you clicked on
			$(this).parent().addClass('open');

			var menu = $(this).parent().find("ul");
			var menupos = menu.offset();
		  
			if ((menupos.left + menu.width()) + 30 > $(window).width()) {
				var newpos = - menu.width();      
			} else {
				var newpos = $(this).parent().width();
			}
			menu.css({ left:newpos });

		});

		$( "li.dropdown" ).hover(
		  function() {
			$( this ).addClass( "open" );
		  },
		  function(){ $(this).removeClass( "open" ) }
		);
	</script>

<?php echo $config_site_gacode; ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='https://embed.tawk.to/589544ac85dc370a6b9723e9/default';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();
// </script>
<!--End of Tawk.to Script-->
</body>
</html>