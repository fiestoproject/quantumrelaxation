<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $config_site_metadescription;?>">
    <meta name="keyword" content="<?php echo $config_site_metakeyword;?>">
    <meta name="author" content="fiesto.com">
	<link href="<?php echo $favicon ?>" rel="SHORTCUT ICON" />
	
    <title><?php echo $config_site_titletag;?></title>
	<?php echo $style_css ?>
    <!-- Bootstrap Core CSS -->
    <link href="<<<TEMPLATE_URL>>>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link type="text/css" rel="stylesheet" href="<<<TEMPLATE_URL>>>/font-awesome/css/font-awesome.min.css">
	<link type="text/css" href="<<<TEMPLATE_URL>>>/js/hoverzoom.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
    <link href="<<<TEMPLATE_URL>>>/css/color.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/styles.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/blog-post.css" rel="stylesheet">
	<link href="<<<TEMPLATE_URL>>>/css/owl.carousel.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/owl.theme.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/owl.transitions.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/animate.min.css" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/hover.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse header1" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
				<div class="container">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
        <div class="header-left">
            <a style="cursor: pointer;">
            <img src="<<<TEMPLATE_URL>>>/images/ico4.png">
            <img src="<<<TEMPLATE_URL>>>/images/ico1.png">
            <img src="<<<TEMPLATE_URL>>>/images/ico2.png">
            <img src="<<<TEMPLATE_URL>>>/images/ico3.png">
            </a>
        </div> 
        <div class="header-right">
            <span><a href="#">2<img src="<<<TEMPLATE_URL>>>/images/cart.png"></a>
<a href="#">LOGIN</a></span>            
        </div>
					<a class="navbar-brand" href="index.html"><?php //echo $dekorasi[0]; ?><img src="<<<TEMPLATE_URL>>>/images/logo.png" alt="logo" /></a>       
				</div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse top-menu" id="bs-example-navbar-collapse-1">
				<div class="container">
					<?php echo $display_menu; ?>
                </div>
			</div><!--/.nav-collapse -->
        <!-- /.container -->
    </nav>

	   <!-- Page Content -->
	<?php if(!$_GET['p']) {?>
		<div class="slider">
			<div id="slide-home" class="owl-carousel owl-theme">
				<div class="item"><?php echo $dekorasi[1] ?></div>
				<div class="item"><?php echo $dekorasi[2] ?></div>
				<div class="item"><?php echo $dekorasi[3] ?></div>
			</div>
		</div>
		<?//php echo getHeader(); ?>
    <div id="services" class="services">
            <div class="container">
                <div class="row text-center">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="service-item">
        <p>We have collection of every season apparel designed by many proffesional fashion dsigner and tendsetter from araound the globe. Loved by woman with high taste of style.</p>
                                </div>
                            </div>
                        </div>
        <!-- /.row (nested) -->
                    </div>
                </div>
            </div>
        </div>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="center"><a href="#">See Collection</a></div>
            </div>
        </div>
</div>
    <?php } ?>
    <div id="content">
    <div class="content-wrapper" id="exclusive-collection">
        <div class="container">
            <div class="row">
            <!-- Blog Post Content Column -->
                <div class="col-md-12">
                    <h1 class="page-header">EXCLUSIVE COLLECTION</h1>
                </div>
            </div>
        </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">  
                        <div class="row">
                        <div class="col-sm-6 col-md-3"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/top1.jpg"></a><p>Uptown Short Sleeve Shirt</p></div>
                        <div class="col-sm-6 col-md-3"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/top2.jpg"></a><p>Midnight Party Dress</p></div>
                        <div class="col-sm-6 col-md-3"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/top2.jpg"></a><p>Midnight Party Dress</p></div>
                        <div class="col-sm-6 col-md-3"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/top3.jpg"></a><p>Casual Sleeveless Dress</p></div>
            <?php //echo $display_main_content_block;?>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="content-wrapper" id="popular-categories">
        <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <h1 class="page-header">POPULAR CATEGORIES</h1>
            </div>
        </div>
        </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/cat4.jpg"></a></div>
                    <div class="col-sm-6 col-md-3"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/cat2.jpg"></a></div>
                    <div class="col-sm-6 col-md-3"><a href="#"><img src="<<<TEMPLATE_URL>>>/images/cat3.jpg"></a></div>
            <?php //echo $display_main_content_block;?>
                </div>
            </div>
        </div>
    </div>
    </div>
              </div>

	<!-- Footer -->

    <footer>
        <div class="footerlink">
			<div class="container">
				<div class="row">
			
				<div class="col-sm-3 col-md-4">
                    <a class="navbar-bran" href="index.html"><?//php echo $dekorasi[0]; ?><img src="<<<TEMPLATE_URL>>>/images/logo.png" alt="logo" /></a>   
                </div>
				<div class="col-sm-3 col-md-2">
                <table>
                    <tbody>
                        <tr>
                        <td><img src="<<<TEMPLATE_URL>>>/images/footer-icon2.png"></td>
                            <td><p>Jl. Darmo no. 23,<br>Surabaya Jawa Timur</p></td>
                        </tr>
                        <tr>
                        <td><img src="<<<TEMPLATE_URL>>>/images/footer-icon1.png"></td>
                            <td><p><a href="#">(031) 555-5252</a></p></td>
                        </tr>
                        <tr>
                        <td><img src="<<<TEMPLATE_URL>>>/images/footer-icon3.png"></td>
                            <td><p><a href="#">open@mind.com</a></p></td>
                        </tr>
                    </tbody>
                </table>
                </div>
				<div class="col-sm-3 col-md-6">
				<div class="container-nav">
					<?//php echo $display_menu; ?>
                    <div class="collapse navbar-collapse top-menu" id="bs-example-navbar-collapse-1">
				<div class="container">
					<ul class="nav navbar-nav">
<li><a href="http://fiesto.off/iwd/dev">Home</a></li>

<li><a href="http://fiesto.off/iwd/dev/catalog/images/31_bedding">How To Buy</a></li>

<li><a href="http://fiesto.off/iwd/dev/catalog/images/32_additional">About Us</a></li>

<li><a href="http://fiesto.off/iwd/dev/contact">Contact Us</a></li>

</ul>
				</div>
			</div>
				</div>
				</div>
				<div class="col-sm-3 col-md-6">
					<h4><strong>Find Us</strong></h4>
<div id="right"><a href="#"><img title="Pinterest" src="<<<TEMPLATE_URL>>>/images/ico4b.png" alt="Pinterest" /></a>&nbsp;<a href="#"> <img title="Facebook" src="<<<TEMPLATE_URL>>>/images/ico1b.png" alt="Facebook" /></a>&nbsp;&nbsp;<a href="#"><img title="Tiwter" src="<<<TEMPLATE_URL>>>/images/ico2b.png" alt="Tiwter" /></a>&nbsp; <a href="#"><img title="Instagram" src="<<<TEMPLATE_URL>>>/images/ico3b.png" alt="Instagram" /></a>&nbsp;</div>
				</div>
				</div>
			</div>
		</div>	
		<div class="copyright">
			<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p>All Right Reserved. <a href="#">Design by Indonesia Web Design</a></p>
				</div>
			</div>
			</div>
		</div>
		<!-- /.row -->
	</footer>

    <!-- Bootstrap Core JavaScript -->
	<script src="<<<TEMPLATE_URL>>>/js/jquery-1.7.2.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/bootstrap.min.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/owl.carousel.min.js"></script>
	<?php
	echo $script_js;
	?>
    <script>
		$(document).ready(function () {
			$('#myCarousel').carousel({
				interval: 10000
			})
			$('.fdi-Carousel .item').each(function () {
				var next = $(this).next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));

				if (next.next().length > 0) {
					next.next().children(':first-child').clone().appendTo($(this));
				}
				else {
					$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
				}
			});
		});
		

    $(document).ready(function() {
     
    $("#product-carousel").owlCarousel({
     
    autoPlay: 3000, //Set AutoPlay to 3 seconds
     
    items : 4,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3],
	navigation : true
     
    });
     
    });
	$(document).ready(function() {
     
    $("#owl-demo1").owlCarousel({
     
    autoPlay: 3000, //Set AutoPlay to 3 seconds
     
    items : 4,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [979,3],
	navigation : true
     
    });
	
     
    });
	$(document).ready(function() {
    $("#slide-home").owlCarousel({
    navigation : true, // Show next and prev buttons
    slideSpeed : 300,
	autoPlay : true,
    paginationSpeed : 400,
	pagination : false,
    singleItem:true
    });
    });
	$(function(){
				$(".nav > li.dropdown > .dropdown-menu > li.dropdown > a").on("click",function(e){
					var current=$(this).next();
					var grandparent=$(this).parent().parent();
					if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
						$(this).toggleClass('right-caret left-caret');
					grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
					grandparent.find(".sub-menu:visible").not(current).show();
					current.toggle();
					e.stopPropagation();
				});
				$(".nav > li.dropdown > .dropdown-menu > li.dropdown > a:not(.trigger)").on("click",function(){
					var root=$(this).closest('.dropdown');
					root.find('.left-caret').toggleClass('right-caret left-caret');
					root.find('.sub-menu:visible').show();
				});
			});
	
$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
    // Avoid following the href location when clicking
    event.preventDefault(); 
    // Avoid having the menu to close when clicking
    event.stopPropagation(); 
    // If a menu is already open we close it
    //$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
    // opening the one you clicked on
    $(this).parent().addClass('open');

    var menu = $(this).parent().find("ul");
    var menupos = menu.offset();
  
    if ((menupos.left + menu.width()) + 30 > $(window).width()) {
        var newpos = - menu.width();      
    } else {
        var newpos = $(this).parent().width();
    }
    menu.css({ left:newpos });

});
    </script>
    
<!-- hover zoom image-->
	<script src="<<<TEMPLATE_URL>>>/js/jquery-1.7.2.min.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="<<<TEMPLATE_URL>>>/js/jquery.selectbox-0.2.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.zoom.js"></script>
	<!-- Property select -->
	<script type="text/javascript">
		jQuery.noConflict()( function($){
			$(function () {
				$("#color-select").selectbox();
			});
		});

		jQuery.noConflict()( function($){
			$(function () {
				$("#size-select").selectbox();
			});
		});

	</script>

	<script type="text/javascript">
		/* Zoom */
			jQuery.noConflict()( function($){
				$(document).ready(function(){
					$('.zoom').zoom();
				});
			});
		/* and Zoom */
	</script>

	<!-- BX Slider launch -->
	<script>
		jQuery.noConflict()( function($){
			$(document).ready(function(){
				$('#shop-slider').bxSlider({
				      pagerCustom: '#shop-slider-pager'
				    });
			});

			$(document).ready(function(){
				$('#shop-slider1').bxSlider({
			      pagerCustom: '#shop-slider-pager1'
			    });
			});

			$(document).ready(function(){
				$('#shop-slider4').bxSlider({
			      pagerCustom: '#shop-slider-pager4'
			    });
			});

			$(document).ready(function(){
				$('#shop-slider7').bxSlider({
			      pagerCustom: '#shop-slider-pager7'
			    });
			});

			$(document).ready(function(){
				$('#shop-slider9').bxSlider({
					pagerCustom: '#shop-slider-pager9'
				});
			});
		});
		$(function() {
$('.carousel-product').owlCarousel({
 
autoPlay: 3000, //Set AutoPlay to 3 seconds
 
items : 3,
itemsDesktop : [1199,4],
itemsDesktopSmall : [980,3],
itemsTablet: [768,3],
itemsTabletSmall: false,
itemsMobile : [479,1],
singleItem : false,
navigation : true
 
});
});
$( "li.dropdown" ).hover(
  function() {
	$( this ).addClass( "open" );
  },
  function(){ $(this).removeClass( "open" ) }
);
	</script>
<!-- hover zoom image ends here -->
<?php echo $config_site_gacode; ?>
</body>
</html>