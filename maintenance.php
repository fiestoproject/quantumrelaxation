<!DOCTYPE html>
<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Audio Quantum Morphic Field Relaxation® (QMFR) diprogram secara spesifik bekerja hanya untuk Anda.">
    <meta name="keyword" content="Audio Quantum Morphic Field Relaxation®  ini sangat istimewa dan efektif membantu meningkatkan kualitas kesehatan dan ketenangan pikiran karena adalah integrasi dari tiga komponen: respon relaksasi, pemberdayaan kesehatan melalui pikiran bawah sadar, dan energi khusus medan morfik untuk kesehatan.">
    <meta name="author" content="fiesto.com">
	<meta name="theme-color" content="#171717">
	<link href="https://quantumrelaxation.id/file/favicon.png" rel="SHORTCUT ICON" />
	
    <title>Quantum Morphic Field Relaxation®</title>
	<!--================================awal Style Catalog=============================================-->
<link type="text/css" href="https://quantumrelaxation.id/modul/catalog/css/basic.css" rel="stylesheet">
<!--================================akhir Style Catalog=============================================--><script>
var pakaicart='1';
var _ADDTOCART='Beli'
</script>
<!--================================Awal Style Webmember=============================================-->
<link type="text/css" href="https://quantumrelaxation.id/modul/webmember/css/basic.css" rel="stylesheet">
<!--================================Akhir Style Webmember=============================================-->
<!--================================awal Style Cart=============================================-->
<link type="text/css" href="https://quantumrelaxation.id/modul/cart/css/basic.css" rel="stylesheet">
<style>
.button-link {
    text-decoration:  none;
    padding: 5px 10px 5px 10px;
    background: #4479BA;
    color: #FFF;
}

.autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-no-suggestion { padding: 2px 5px;}
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: bold; color: #000; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { font-weight: bold; font-size: 16px; color: #000; display: block; border-bottom: 1px solid #000; }
</style>
<!--================================akhir Style Cart=============================================-->

	<script>
	var cfg_app_url='https://quantumrelaxation.id';
	var cfg_template_url='https://quantumrelaxation.id/template/luxqueen';
	</script>
	
    <!-- Bootstrap Core CSS -->
    <link href="https://quantumrelaxation.id/template/luxqueen/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
	<link type="text/css" rel="stylesheet" href="https://quantumrelaxation.id/template/luxqueen/font-awesome/css/font-awesome.min.css">
	<link type="text/css" href="https://quantumrelaxation.id/template/luxqueen/js/hoverzoom.css" rel="stylesheet">
	<!--<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>-->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,400italic,600italic,800' rel='stylesheet' type='text/css'>
    <link href="https://quantumrelaxation.id/template/luxqueen/css/color.css" rel="stylesheet">
    <link href="https://quantumrelaxation.id/template/luxqueen/css/styles.css" rel="stylesheet">
    <link href="https://quantumrelaxation.id/template/luxqueen/css/blog-post.css?ver=2.6" rel="stylesheet">
	<link href="https://quantumrelaxation.id/template/luxqueen/css/owl.carousel.css" rel="stylesheet">
    <link href="https://quantumrelaxation.id/template/luxqueen/css/owl.theme.css" rel="stylesheet">
    <link href="https://quantumrelaxation.id/template/luxqueen/css/owl.transitions.css" rel="stylesheet">
	<link href="https://quantumrelaxation.id/js/mp/magnific-popup.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style type="text/css">
	@media(min-width:768px){
		.dekstop-hidden{display:none !important}
	}
	@media(max-width:767px){
		.mobile-hidden{display:none !important}
	}
		@media (max-width:767px){
	.navbar-header {
	}
	}
					</style>
	<script src="https://quantumrelaxation.id/template/luxqueen/js/jquery-1.7.2.min.js"></script>
	<script src="https://quantumrelaxation.id/js/mp/jquery.magnific-popup.min.js"></script>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse header1" role="navigation">
		<div class="container">
			<!--<div class="header-right">
				<ul class="topbar-nav login-menu" id="b-nav">
					<li class="public-li"> 
						<a class="cart" href="https://quantumrelaxation.id/cart">
							<span>0</span>
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</a>
					</li>
				</ul> 					
			</div>-->
			<div class="top-header">
				<div class="block-login">
					<ul class="topbar-nav login-menu list-inline" id="b-nav">
											</ul>
				</div>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="navbar-header">
				<a class="navbar-brand dekstop-hidden" href="https://quantumrelaxation.id"><img src="https://quantumrelaxation.id/file/dekorasi/headerlogomobile.png" border="0" /></a>       
				<a class="navbar-brand mobile-hidden" href="https://quantumrelaxation.id"><img src="https://quantumrelaxation.id/file/dekorasi/headerlogo.png" border="0" /></a>       
			</div>
			
			<div class="social-header">
				<table>
<tbody>
<tr>
<td><a href="https://adiwgunawan.com/"><img class="dekstop-hidden" style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/icon adiW.svg" alt="icon adi w gunawan" /> <img class="mobile-hidden" style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/icon adiW long.jpg?v=1" alt="icon adi w gunawan" /></a> <a href="https://theheart.or.id/"><img class="dekstop-hidden" style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/logo-THT.png" alt="icon theheart" /> <img class="mobile-hidden" style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/icon theheart long.jpg?v=1" alt="icon theheart" /></a> <a href="https://www.facebook.com/AdiWGunawan"><img style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/icon FB.svg" alt="icon facebook" /></a> <a href="https://www.youtube.com/c/AdiWGunawanOfficial"><img style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/icon YT.svg" alt="icon youtube" /></a> <a href="https://www.instagram.com/adiwgunawan.id/"><img style="height: 40px;" src="https://quantumrelaxation.id/file/media/source/icon/icon IG.svg" alt="icon instagram" /></a></td>
</tr>
</tbody>
</table>			</div>
		</div>
    </nav>
	 
		
		<div id="services" class="services"><div class="container">		
		<div class="row">
				<div class="col-sm-12">
					<div class="service-item">
						<p style="text-align: center;font-size:20pt;"><strong>
						Mohon maaf atas ketidaknyamanannya. 
						Website sedang maintenance untuk memberikan pelayanan terbaik untuk anda.
						</strong></p>
					</div>
				</div>
				
			</div>			
		</div>
		</div>      
    <footer>
		
		<div class="footerlink">
			<div class="container">
				<div class="row">
			
				<div class="col-sm-4">
					<p><img src="https://quantumrelaxation.id/file/media/source/logo-footer.png" alt="logo-footer" /></p>
				</div>
				<div class="col-sm-3">
					<table>
<tbody>
<tr>
<td><img src="https://quantumrelaxation.id/file/media/source/map.png" alt="map" /></td>
<td>&nbsp;</td>
<td>Jl. Argopuro no. 43<br />Surabaya, Jawa Timur</td>
</tr>
<tr>
<td><img src="https://quantumrelaxation.id/file/media/source/phone.png" alt="phone" /></td>
<td>&nbsp;</td>
<td><a href="tel:+62315461827">(031) 546 1827</a>&nbsp;/&nbsp;<a href="tel:+62315470437">547 0437</a></td>
</tr>
<tr>
<td><img src="https://quantumrelaxation.id/file/media/source/mail.png" alt="mail" /></td>
<td>&nbsp;</td>
<td><a href="mailto:cs@adiwgunawan.com">cs@adiwgunawan.com</a></td>
</tr>
</tbody>
</table>
				</div>
				<div class="col-sm-5">
			
<p>&nbsp;</p>
<table>
<tbody>
<tr>
<td>
<h4>FIND US</h4>
</td>
<td><a href="https://www.facebook.com/AdiWGunawan"><img style="width: 42px;" src="https://quantumrelaxation.id/file/media/source/sosmed FB.png" alt="sosmed FB" /></a>&nbsp; &nbsp;<a href="https://www.youtube.com/c/AdiWGunawanOfficial"><img style="width: 42px;" src="https://quantumrelaxation.id/file/media/source/sosmed YT.png" alt="sosmed YT" /></a>&nbsp; &nbsp;<a href="https://www.instagram.com/adiwgunawan.id/"><img style="width: 42px;" src="https://quantumrelaxation.id/file/media/source/sosmed IG.png" alt="sosmed IG" /></a></td>
</tr>
</tbody>
</table>
				</div>
				</div>
			</div>
		</div>		<div class="copyright">
			<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p>Powered by <a href="https://www.fiesto.com">fiesto.com</a></p>				</div>
			</div>
			</div>
		</div>
		<!-- /.row -->
	</footer>
	<div id="button-mobile">
		<div class="menu-button-mobile">
			<ul class="list-inline">
				<li><a href="https://quantumrelaxation.id"><span class="button-home-icon"></span></a></li>
				<!--<li><a href="mailto:cs@TheHeart.or.id"><span class="button-menu-icon"></span></a></li>-->
				<li><a href="tel:+62315461827"><span class="button-phone-icon"></span></a></li>
				<li><a href="https://wa.me/+628983449650"><span class="button-mail-icon"></span></a></li>
				<li><a href="https://quantumrelaxation.id/page/view/11_produk"><span class="button-product-icon"></span></a></li>
			</ul>
		</div>
	</div>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
    <!-- Bootstrap Core JavaScript -->
	<!--<script src="https://quantumrelaxation.id/template/luxqueen/js/jquery-1.7.2.min.js"></script>-->
    <script src="https://quantumrelaxation.id/template/luxqueen/js/bootstrap.min.js"></script>
	<script src="https://quantumrelaxation.id/template/luxqueen/js/owl.carousel.min.js"></script>
	<script src="https://quantumrelaxation.id/template/luxqueen/js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="https://quantumrelaxation.id/template/luxqueen/js/jquery.selectbox-0.2.js"></script>
	<script src="https://quantumrelaxation.id/template/luxqueen/js/jquery.zoom.js"></script>
	<script src="https://quantumrelaxation.id/template/luxqueen/js/isotope.pkgd.min.js"></script>
		
		<script type="text/javascript">
			$(function() {
				$('.carousel-testimonials').owlCarousel({
				 
					autoPlay: 8000, //Set AutoPlay to 3 seconds
					items : 3,
					itemsDesktop : [1199,3],
					itemsDesktopSmall : [979,3],
					navigation : true
				 
				});
			});
		</script>
		
<script type="text/javascript" src="https://quantumrelaxation.id/js/zoom/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="https://quantumrelaxation.id/modul/catalog/js/catalog.js"></script>
<script>
	$(function() {
		$(".qty-trigger-detail").click(function() {
			var ID = $(this).attr("data-id");
			var room_qty = parseInt($("#quantity_wanted_"+ID).val());
			var id_tombol = $(this).attr("id");
			var qty_max = parseInt($("#quantity_wanted_"+ID).attr("data-max"));

			if(id_tombol == "qtyplus") {
				if (room_qty >= qty_max) {
					alert('Batas limit');
				} else {
					room_qty++;
				}
			} else if(room_qty > 1) {
				room_qty--;
			}
			
			$("#quantity_wanted_"+ID).val(room_qty);
			$("#lblcart_quantity_input_detail_"+ID).html(room_qty);
		});
	})
</script>
<script src="https://quantumrelaxation.id/js/jquery.validate.min.js"></script>
<script>
	function refreshCaptcha()
	{
		var img = document.images["captchaimg"];
		img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
	}
	$(document).ready(function()
	{
		$(".menu-edit-password > a").click(function()
		{
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
		});
		
		$('#signupform').validate({
			rules: {
				txtEmail: {
					required: true,
					email: true
				},
				txtNama: {
					required: true,
					minlength: 3
				},
				txtPass1: {
					required: true,
					minlength: 6
				},
				txtPass2: {
					required: true,
					minlength: 6,
					equalTo: "#txtPass1"
				},
				txtAlamat1: {
					required: true,
					minlength: 3
				},
				txtTelepon: {
					required: true,
					number: true
				}/* ,
				txtPonsel1: {
					required: true,
					number: true
				} */
			}, 
			messages: {
				txtPass2: "Please enter the same password as above",
			}
		})
	});
</script>
<script src="https://quantumrelaxation.id/js/accounting.min.js"></script>
<script src="https://quantumrelaxation.id/js/jquery.autocomplete.min.js"></script>
<script src="https://quantumrelaxation.id/js/jquery.validate.min.js"></script>
<script>

	$(function() {
		$("#checkout-form").validate({
			submitHandler: function(form) {
				form.submit();
			}
		});
		
		var total = 0;
		var amount = 0;
		$('.cart_quantity_input').each(function() {
			items = parseInt($(this).val());
			total += items;
		})
		
		$(".qty-trigger").click(function() {
			var ID = $(this).attr("data-id");
			var room_qty = parseInt($("#cart_quantity_input_"+ID).val());
			var id_tombol = $(this).attr("id");
			var qty_max = parseInt($("#cart_quantity_input_"+ID).attr("data-max"));
			var quantityUpdated = $("#cart_quantity_input_"+ID).attr('name');
			var product_id = $("#product_id_"+ID).val();
			var closes = $(this).closest('tr');
			var price_now = closes.find('.price-now').attr('data-price');
			var price = closes.find('.line-price');
			var data_line_price = closes.find('.line-price').attr('data-line-price');
			var total_price = $('#total_price');
			var amount_price = $('#amount_price');
			var line_cost = 0;
			
			if(id_tombol == "qtyplus") {
				if (room_qty >= qty_max) {
					alert('Batas limit');
				} else {
					room_qty++;
				}
			} else if(room_qty > 1) {
				room_qty--;
			}
			
			line_cost = price_now * room_qty;
			price.text(accounting.formatNumber(line_cost, 0, '.', ','));
			price.attr('data-line-price', line_cost);
			
			$("#cart_quantity_input_"+ID).val(room_qty);
			$("#lblcart_quantity_input_"+ID).html(room_qty);
			
			var j = 1;
			$.post("https://quantumrelaxation.id/modul/cart/ajax.php", {action: 'updateCart', product_id: product_id, quantity: room_qty, updated: quantityUpdated}, function( data ) {
			  
				if(id_tombol == "qtyplus") {
					total += j;
				} else if(room_qty > 1) {
					total -= j;
				}
				$('#b-nav').find('span').text(total);
			});
			
			update_amounts();
		});
		

	});
	
	
	function update_amounts() {
		var sum = 0;
		$('.cart_item').each(function() {
			var qty = parseInt($(this).find('.lblcart_quantity_input').text());
			var price = parseInt($(this).find('.price-now').attr('data-price'));
			var amount = qty * price;
			
			sum += amount;
		})
		
		$('#amount_price').val(sum);
		$('#total_price').text(accounting.formatNumber(sum, 0, '.', ','));
	}
	
	function format_rupiah(str) {
		var symbol = '';
		return accounting.formatMoney(str, symbol, 0, ".", ",");
	}	

	function add_attr_req(elem) {
		elem.attr('required', true);
	}
	
	function remove_attr_req(elem) {
		elem.removeAttr('required');
	}
	
</script>
	
	<script>
		$( document ).ready(function() {
			var sync1 = $("#sync1");
			  var sync2 = $("#sync2");

			  sync1.owlCarousel({
				singleItem : true,
				slideSpeed : 1000,
				navigation: true,
				pagination:false,
				afterAction : syncPosition,
				responsiveRefreshRate : 200,
			  });

			  sync2.owlCarousel({
				items : 9,
				itemsDesktop      : [1199,9],
				itemsDesktopSmall     : [979,9],
				itemsTablet       : [768,7],
				itemsMobile       : [479,7],
				pagination:false,
				responsiveRefreshRate : 100,
				afterInit : function(el){
				  el.find(".owl-item").eq(0).addClass("synced");
				}
			  });

			  function syncPosition(el){
				var current = this.currentItem;
				$("#sync2")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync2").data("owlCarousel") !== undefined){
				  center(current)
				}

			  }

			  $("#sync2").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			  });

			  function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;

				var num = number;
				var found = false;
				for(var i in sync2visible){
				  if(num === sync2visible[i]){
					var found = true;
				  }
				}

				if(found===false){
				  if(num>sync2visible[sync2visible.length-1]){
					sync2.trigger("owl.goTo", num - sync2visible.length+2)
				  }else{
					if(num - 1 === -1){
					  num = 0;
					}
					sync2.trigger("owl.goTo", num);
				  }
				} else if(num === sync2visible[sync2visible.length-1]){
				  sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
				  sync2.trigger("owl.goTo", num-1)
				}
			  }
			$('#search-icon').on('click', function(){
				$('#searchproduct').toggleClass('active');
			});
			$('#close-search-icon').on('click', function(){
				$('#searchproduct').removeClass('active');
			});
			$("#popular-categories img").css("min-height",$("#popular-categories img").height());	
			$('.button-menu-fixed').on('click', function(e) {
			  $('#bs-example-navbar-collapse-3').toggleClass("show");
			  e.preventDefault();
			});
			$('.button-mobile-toggle').on('click', function(e) {
			  $('#button-mobile').toggleClass("nonactive");
			  $('body').toggleClass("mobile-display");
			  e.preventDefault();
			});
			$(".megamenu").each(function(index)
			{
				var find_th = $(this).find(".menu-li-standart");
				if(find_th.length > 0)
				{
					$(this).addClass("menu-standart");
				}
			});
		});
		$(function() {
			
			/*$(".image-product").css("padding-top",$(".caption").height());*/
			$('#myCarousel').carousel({
				interval: 10000
			})
			$('.fdi-Carousel .item').each(function () {
				var next = $(this).next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));

				if (next.next().length > 0) {
					next.next().children(':first-child').clone().appendTo($(this));
				}
				else {
					$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
				}
			});
			
			$("#product-carousel").owlCarousel({
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				items : 4,
				itemsDesktop : [1199,4],
				itemsDesktopSmall : [979,3],
				navigation : true
			 
			});
			
			$("#owl-demo1").owlCarousel({
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				items : 4,
				itemsDesktop : [1199,3],
				itemsDesktopSmall : [979,3],
				navigation : true
			 
			});	
			
			$("#slide-home").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,	transitionStyle : "fade",	
				autoPlay: 9000, //Set AutoPlay to 3 seconds
				paginationSpeed : 400,
				pagination : false,
				singleItem:true
			});
			
			$(".nav > li.dropdown > .dropdown-menu > li.dropdown > a").on("click",function(e){
				var current=$(this).next();
				var grandparent=$(this).parent().parent();
				if($(this).hasClass('left-caret')||$(this).hasClass('right-caret'))
					$(this).toggleClass('right-caret left-caret');
				grandparent.find('.left-caret').not(this).toggleClass('right-caret left-caret');
				grandparent.find(".sub-menu:visible").not(current).show();
				current.toggle();
				e.stopPropagation();
			});
			$(".nav > li.dropdown > .dropdown-menu > li.dropdown > a:not(.trigger)").on("click",function(){
				var root=$(this).closest('.dropdown');
				root.find('.left-caret').toggleClass('right-caret left-caret');
				root.find('.sub-menu:visible').show();
			});	
			
			$('.zoom').zoom();
			$('#shop-slider').bxSlider({
				pagerCustom: '#shop-slider-pager'
			});
			$('#shop-slider1').bxSlider({
				pagerCustom: '#shop-slider-pager1'
			});	
			$('#shop-slider4').bxSlider({
				pagerCustom: '#shop-slider-pager4'
			});	
			$('#shop-slider7').bxSlider({
				pagerCustom: '#shop-slider-pager7'
			});	
			$('#shop-slider9').bxSlider({
				pagerCustom: '#shop-slider-pager9'
			});	
			
			$('.carousel-product').owlCarousel({
			 
				autoPlay: 3000, //Set AutoPlay to 3 seconds
				 
				items : 5,
				itemsDesktop : [1199,5],
				itemsDesktopSmall : [980,4],
				itemsTablet: [768,3],
				itemsTabletSmall: false,
				itemsMobile : [479,2],
				singleItem : false,
				navigation : true
			 
			});	
			
			
			// init Isotope
			var $grid = $('.grid').isotope({
				itemSelector: '.element-item',
				layoutMode: 'fitRows',
				getSortData: {
					name: '.name',
					symbol: '.symbol',
					number: '.number parseInt',
					category: '[data-category]',
					weight: function( itemElem ) {
						var weight = $( itemElem ).find('.weight').text();
						return parseFloat( weight.replace( /[\(\)]/g, '') );
					}
				}
			});
			
			// filter functions
			var filterFns = {
				// show if number is greater than 50
				numberGreaterThan50: function() {
				var number = $(this).find('.number').text();
					return parseInt( number, 10 ) > 50;
				},
				// show if name ends with -ium
				ium: function() {
					var name = $(this).find('.name').text();
					return name.match( /ium$/ );
				}
			};

			// bind filter button click
			$('#filters').on( 'click', 'button', function() {
				var filterValue = $( this ).attr('data-filter');
				// use filterFn if matches value
				filterValue = filterFns[ filterValue ] || filterValue;
				$grid.isotope({ filter: filterValue });
			});			
			
			$('.popup-youtube').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,

				fixedContentPos: false
			});
			
		})

		$('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
			// Avoid following the href location when clicking
			event.preventDefault(); 
			// Avoid having the menu to close when clicking
			event.stopPropagation(); 
			// If a menu is already open we close it
			//$('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
			// opening the one you clicked on
			$(this).parent().addClass('open');

			var menu = $(this).parent().find("ul");
			var menupos = menu.offset();
		  
			if ((menupos.left + menu.width()) + 30 > $(window).width()) {
				var newpos = - menu.width();      
			} else {
				var newpos = $(this).parent().width();
			}
			menu.css({ left:newpos });

		});

		$( "li.dropdown" ).hover(
		  function() {
			$( this ).addClass( "open" );
		  },
		  function(){ $(this).removeClass( "open" ) }
		);
	</script>

<script>

</script><!--Start of Tawk.to Script-->
<script type="text/javascript">
// var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
// (function(){
// var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
// s1.async=true;
// s1.src='https://embed.tawk.to/589544ac85dc370a6b9723e9/default';
// s1.charset='UTF-8';
// s1.setAttribute('crossorigin','*');
// s0.parentNode.insertBefore(s1,s0);
// })();
// </script>
<!--End of Tawk.to Script-->
</body>
</html>